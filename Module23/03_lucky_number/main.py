import random
total_sum = 0

with open('numbers.txt', 'w') as file:
    while total_sum <= 776:
        try:
            user_number = int(input('Введите число: '))
            random_error = random.randint(1, 13)
            if random_error == 5:
                raise TimeoutError('Тебе не повезло. Программа завершилась.')
            total_sum += user_number
            file.write('{}\n'.format(user_number))
        except ValueError:
            print('Введёные данные не являються числом')
