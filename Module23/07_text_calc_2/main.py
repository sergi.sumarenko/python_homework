calc_operations = {
    '+': (lambda x, y: x + y),
    '-': (lambda x, y: x - y),
    '*': (lambda x, y: x * y),
    '/': (lambda x, y: x / y),
    '//': (lambda x, y: x // y),
    '%': (lambda x, y: x % y),
    '**': (lambda x, y: x ** y)
}
total_sum = 0


def create(data, database):
    try:
        data_lst = data.split()
        res = database[data_lst[1]](int(data_lst[0]), int(data_lst[2]))
    except (ValueError, IndexError, KeyError, TypeError, AttributeError):
        print('Обнаружена ошибка в строке: {}'.format(data))
        res = correct()
    return res


def correct():
    while True:
        user_answer = input('Хотите исправить? Да/нет: ').lower()
        if user_answer == 'нет':
            return 0
        elif user_answer == 'да':
            new_line = input('Введите исправленную строку: ')
            return create(new_line, calc_operations)
        else:
            print('Ошибка ввода')


try:
    with open('calc.txt', 'r') as calc:
        for line in calc:
            total_sum += create(line, calc_operations)
except FileNotFoundError:
    print('Файл не найден')

print(total_sum)
