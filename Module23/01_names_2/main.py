name_symbols_symm = 0

with open('people.txt', 'r') as file:
    with open('error.txt', 'w') as error:
        for i_line, line in enumerate(file):
            try:
                if len(line) - 1 < 3:
                    raise ValueError
                name_symbols_symm += len(line) - 1
            except ValueError:
                print('В строке {} недостаточно символов.'.format(i_line))
                error.write('ValueError: В имени "{}" меньше 3х символов.\n'.format(line.rstrip('\n')))

print('Сумма символов имён, отвечающих требованиям:', name_symbols_symm)