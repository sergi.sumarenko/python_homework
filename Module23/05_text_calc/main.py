calc_operations = {
    '+': (lambda x, y: x + y),
    '-': (lambda x, y: x - y),
    '*': (lambda x, y: x * y),
    '/': (lambda x, y: x / y),
    '//': (lambda x, y: x // y),
    '%': (lambda x, y: x % y),
    '**': (lambda x, y: x ** y)
}
total_sum = 0

with open('calc.txt', 'r') as calc:
    for i_line, line in enumerate(calc):
        elem = line.split()
        try:
            total_sum += calc_operations[elem[1]](int(elem[0]), int(elem[2]))
        except ValueError:
            print('В строке №{}: {} операнд(ы) не является целым числом.'.format(i_line + 1, line))
        except IndexError:
            print('В строке №{}: {} не хватает элементов.'.format(i_line + 1, line))
        except KeyError:
            print('В строке №{}: {} неизвестный оператор.'.format(i_line + 1, line))

print(total_sum)
