import socket, os, threading
client_list = list()


def server():
    while True:
        client, addr = sock.accept()
        client_list.append(client)
        listen_client = threading.Thread(target=data_processing, args=(client, addr), daemon=True)
        listen_client.start()


def data_processing(conn, ip):
    print('Новый клиент подключен:', ip)
    while True:
        data = conn.recv(1024)
        decode_data = data.decode()
        head = decode_data.split()[0]
        if head == 'r0x0101010m':
            with open('chat.txt', 'r') as read_chat:
                conn.send(('r0x0101010m ' + read_chat.read()).encode())
        elif head == 'q0x0606060m':
            client_list.remove(conn)
            conn.close()
            print('Клиент {} отключился.'.format(ip[0]))
            break
        elif head == 's0x1010101m':
            with open('chat.txt', 'a') as write_chat:
                write_chat.write('\n{}'.format(decode_data[12:]))
            for clt in client_list:
                if clt != conn:
                    clt.send(('s0x1010101m ' + 'Сообщение от ' + decode_data[12:]).encode())


sock = socket.socket()
sock.bind(('127.0.0.1', 55000))
sock.listen()
print('Сервер включен.')

if not os.path.exists('chat.txt'):
    with open('chat.txt', 'w') as chat_file:
        chat_file.write('Весь чат:'.ljust(25))

server_work = threading.Thread(target=server, daemon=True)
server_work.start()

server_question = input('\n1. Quit\n')
if server_question == '1':
    for people in client_list:
        people.send(b'q0x0606060m')
        people.close()
    print('\nСервер выключен.')
else:
    print('Ошибка ввода')
