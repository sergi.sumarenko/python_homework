import socket, time, threading


def display_chat():
    while True:
        data = sock.recv(1024)
        decode_data = data.decode()
        head = decode_data.split()[0]
        if head == 'q0x0606060m':
            return print('Сервер упал. Данных больше не будет.\n~ ', end='')
        else:
            for line in decode_data[12:].split('\n'):
                print('\n{}'.format(line), end='')
            if head == 's0x1010101m':
                print('\nВвод: ')


def action_background():
    print('\n' + ('+' + '-' * 25) * 3 + '+')
    print('|{: ^25}|{: ^25}|{: ^25}|'.format(
        '1. Просмотреть чат',
        '2. Написать сообщение',
        '3. Выйти'
    ))
    print(('+' + '-' * 25) * 3 + '+')


def menu(name, client_socket):
    while True:
        action_background()
        action = input('\nВвод: ')
        if action == '1':
            client_socket.send('r0x0101010m '.encode())
            time.sleep(0.01)
        elif action == '2':
            message = input('\n***{} введите сообщение: '.format(name))
            client_socket.send(bytes('s0x1010101m ' + name + ':\n\t' + message, encoding='UTF-8'))
        elif action == '3':
            print('Соединение с сервером разорвано.')
            break
        else:
            print('команда не найдена.')


user_name = input('Введите имя: ')
sock = socket.socket()

try:
    sock.connect(('127.0.0.1', 55000))
except ConnectionRefusedError:
    print('Соединение не было установлено')
else:
    print('Соединение установлено.')
    th = threading.Thread(target=display_chat, daemon=True)
    th.start()
    menu(user_name, sock)
    try:
        sock.send('q0x0606060m '.encode())
    except BrokenPipeError:
        ...
finally:
    sock.close()
