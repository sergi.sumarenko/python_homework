def check_info(i_data, data):
    data = data.split()
    try:
        if len(data) != 3:
            raise IndexError
        elif not data[0].isalpha():
            raise NameError
        elif '@' not in data[1] and '.' not in data[1]:
            raise SyntaxError('Ошибка в email')
        elif not 10 < int(data[2]) < 99:
            raise ValueError
        else:
            good_database.write(data)
    except IndexError:
        dab_database.write('IndexError: Нехватает элементов в строке {}, "{}".\n'.format(
            i_data + 1,
            ' '.join(data)
        ))
    except NameError:
        dab_database.write('IndexError: В имени "{}" присутствуют не только буквы, в строке {}.\n'.format(
            data[0],
            i_data + 1
        ))
    except SyntaxError:
        dab_database.write('IndexError: Отсутствует @ или . в email "{}", в строке {}.\n'.format(
            data[1],
            i_data + 1
        ))
    except ValueError:
        dab_database.write('IndexError: Возвраст "{}" не является числом'
                           'или не входит в заданый диапазон является числом {}.\n'.format(
                            data[2],
                            i_data + 1
                            ))


with open('registrations.txt', 'r') as database:
    with open('registrations_good.log', 'w') as good_database:
        with open('registrations_bad.log', 'w') as dab_database:
            for i_line, line in enumerate(database):
                check_info(i_line, line)
