import random

max_number = int(input('Введите максимальное число: '))

artem_num = str(random.randint(1, max_number))

first_multitudes = {str(num) for num in range(1, max_number + 1)}

while True:
    question = input('Нучжное число есть среди этих чисел: ')
    if question == 'помогите':
        print('Артём мог загадать следующее число:', end=' ')
        for num in first_multitudes:
            print(num, end=' ')
        print()
    elif question == str(artem_num):
        print('Ты угадал!')
        break
    elif artem_num in question:
        print('Ответ Артёма: Да')
        first_multitudes &= set(question.split())
    else:
        print('Ответ Артёма: Нет')
        first_multitudes -= set(question.split())

# зачтено
