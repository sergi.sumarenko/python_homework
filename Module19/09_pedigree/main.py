quantity = int(input('Введите число человек: '))

trees_dict = {}
height_dict = {}

for num in range(quantity - 1):
    pair = input('{} пара: '.format(num + 1)).split()
    trees_dict[pair[0]] = pair[1]
    height_dict[pair[0]] = 0
    height_dict[pair[1]] = 0

for name in trees_dict:
    m_name = name
    while m_name in trees_dict:
        m_name = trees_dict[m_name]
        height_dict[name] += 1

for i in sorted(height_dict):
    print(i, height_dict[i])

# зачтено
