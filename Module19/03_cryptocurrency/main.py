def dict_print():
    for i_key in data:
        if isinstance(data[i_key], list):
            print(i_key + ':')
            for i_list in range(len(data[i_key])):
                for key_lvl2 in data[i_key][i_list]:
                    if isinstance(data[i_key][i_list][key_lvl2], dict):
                        print('\n\t', key_lvl2 + ':')
                        for key_lvl3 in data[i_key][i_list][key_lvl2]:
                            print('\t\t', key_lvl3 + ': ' + str(data.get(i_key)[i_list].get(key_lvl2).get(key_lvl3)))
                    else:
                        print('\t', key_lvl2 + ': ' + str(data.get(i_key)[i_list].get(key_lvl2)))
        elif isinstance(data[i_key], dict):
            print(i_key + ':')
            for key_lvl2 in data[i_key]:
                print('\t', key_lvl2 + ': ' + str(data.get(i_key).get(key_lvl2)))
        else:
            print(i_key + ': ' + str(data.get(i_key)))
        print()


data = {
    "address": "0x544444444444",
    "ETH": {
        "balance": 444,
        "total_in": 444,
        "total_out": 4
    },
    "count_txs": 2,
    "tokens": [
        {
            "fst_token_info": {
                "address": "0x44444",
                "name": "fdf",
                "decimals": 0,
                "symbol": "dsfdsf",
                "total_supply": "3228562189",
                "owner": "0x44444",
                "last_updated": 1519022607901,
                "issuances_count": 0,
                "holders_count": 137528,
                "price": False
            },
            "balance": 5000,
            "totalIn": 0,
            "total_out": 0
        },
        {
            "sec_token_info": {
                "address": "0x44444",
                "name": "ggg",
                "decimals": "2",
                "symbol": "fff",
                "total_supply": "250000000000",
                "owner": "0x44444",
                "last_updated": 1520452201,
                "issuances_count": 0,
                "holders_count": 20707,
                "price": False
            },
            "balance": 500,
            "totalIn": 0,
            "total_out": 0
        }
    ]
}

dict_print()

data['ETH']['total_diff'] = 100
data['tokens'][0]['fst_token_info']['name'] = 'doge'
data['ETH']['totalOut'] = data['tokens'][0].pop('total_out') + data['tokens'][1].pop('total_out')
data['tokens'][1]['sec_token_info']['total_price'] = data['tokens'][1]['sec_token_info'].pop('price')

dict_print()

# зачтено
