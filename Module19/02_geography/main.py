amount_country = int(input('Введите количество стран: '))
country_dict = {}
for i_country in range(amount_country):
    country = input('Введите данные о {}-й стране (через пробел): '.format(i_country + 1)).split()
    country_dict[country[0]] = set(country[1:])

for i_city in range(1, 4):
    city = input('Введите название {}-го города: '.format(i_city))
    for place in country_dict:
        if city in country_dict[place]:
            print('Город {0} находится в стране {1}'.format(city, place))
            break
    else:
        print('По городу {} данных нет.'.format(city))

# зачтено
