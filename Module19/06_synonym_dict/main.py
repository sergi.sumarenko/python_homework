synonym_dict = {}
check = False

dict_length = int(input('Введите количество пар слов: '))

for num in range(1, dict_length + 1):
    synonyms = input('Enter the synonyms: ').lower().split(' - ')
    synonym_dict[num] = {word for word in synonyms}

while True:
    word = input('Enter the word: ').lower()
    for syn in synonym_dict.values():
        if word in syn:
            print('Синоним:', ''.join(syn - {word}).capitalize())
            check = True
            break
    if check:
        break
    print('Такого слова в словаре нет.')

# зачтено
