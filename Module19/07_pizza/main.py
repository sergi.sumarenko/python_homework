sort_order_dict = {}

quantity = int(input('Ввкдитк кол-во заказов: '))

order_dict = {'{} заказ'.format(i_q + 1): input('{} заказ: '.format(i_q + 1)).split()
              for i_q in range(quantity)}

for val in order_dict.values():
    if val[0] in sort_order_dict.keys():
        if val[1] in sort_order_dict[val[0]]:
            sort_order_dict[val[0]][val[1]] += int(val[2])
        else:
            sort_order_dict[val[0]][val[1]] = int(val[2])
    else:
        sort_order_dict[val[0]] = {val[1]: int(val[2])}

for key, value in sorted(sort_order_dict.items()):
    print('{}:'.format(key))
    length = ' ' * (len(key) + 1)
    for k_lvl2, v_lvl2 in sorted(value.items()):
        print('{0}{1}: {2}'.format(length, k_lvl2, v_lvl2))

# зачтено
