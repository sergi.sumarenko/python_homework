string = input('Enter the string: ').lower()

length = len(string) % 2
check = 0

sym_dict = {sym: string.count(sym) for sym in string}

for v in sym_dict.values():
    if v % 2 != 0:
        check += 1

if ((length == 0) and (check == 0)) or ((length != 0) and (check < 2)):
    print('Можно сделать палиндром')
else:
    print('Нельзя сделать палиндром')
print(sym_dict)

# зачтено
