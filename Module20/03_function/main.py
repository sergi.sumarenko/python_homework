def search(data, element):
    check_element = [index for index, sym in enumerate(data) if sym == element]
    if len(check_element) == 0:
        new_tuple = ()
    elif len(check_element) == 1:
        new_tuple = (sym for sym in data[check_element[0]:])
    else:
        new_tuple = (sym for sym in data[check_element[0]:check_element[1] + 1])
    return new_tuple


something_tuple = (1, 2, 3, 4, 5, 6, 7, 8, 2, 2, 9, 10)

user_element = 7

print(tuple(search(something_tuple, user_element)))

# зачтено
