def first_solution(data):
    start = 0
    stop = 2
    new_list = []
    for _ in range(len(data) // 2):
        new_list.append(tuple(data[start:stop]))
        start += 2
        stop += 2
    return new_list


def second_solution(data):
    new_list = []
    copy_list = data[:]
    for _ in range(len(data) // 2):
        new_list.append(tuple(copy_list.pop(0) for i_ in range(2)))
    return new_list


original_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

print(first_solution(original_list))
print(second_solution(original_list))

# зачтено
