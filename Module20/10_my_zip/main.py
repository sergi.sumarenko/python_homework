def my_zip(first_object, second_object):
    second_object = [v for _, v in enumerate(second_object)]
    first_object = [v for _, v in enumerate(first_object)]
    object_end = min(len(first_object), len(second_object))
    new_tuple = ((first_object[i_element], second_object[i_element]) for i_element in range(object_end))
    return new_tuple


string = 'abcd'
num_tuple = (10, 20, 30)
result = my_zip(string, num_tuple)
print(result)
for i in result:
    print(i)

# зачтено
