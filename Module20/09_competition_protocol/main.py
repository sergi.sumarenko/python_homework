def check_record(number, data):
    while True:
        record = input('{} запись:   '.format(number)).split()
        if not record[0].isdigit():
            print('Ошибка ввода. Результат игрока может содержать только цифры')
        elif len(record) != 2:
            print(
                'Ошибка ввода. Запись должна содержать 2 значения - результат, имя '
                '(примечание: имя не может содержать "пробел", замените на "_")'
            )
        else:
            if record[1] not in data.keys():
                data[record[1]] = [int(record[0])]
            else:
                data[record[1]].append(int(record[0]))
            break


def menu_ch_pl(data, number):
    menu = input('Добавить запись в протокол(1)\nЗавершить(2)\n')
    if menu == '1':
        check_record(number, data)
        number += 1
        menu_ch_pl(data, number)
    elif menu == '2':
        menu_check = False
        return menu_check
    else:
        print('Ошибка ввода. Такой команды не существует.')
        menu_ch_pl(data, number)


def check_players(data, number):
    menu_check = True
    number += 1
    while True:
        if len(total_registry.keys()) < 3:
            print('Ошибка входных данных. Количество игроков меньше 3.')
            if menu_check:
                menu_check = menu_ch_pl(data, number)
            else:
                return False
        else:
            return True


def one_champion(winner, data):
    for k_name, v in data.items():
        if winner in v:
            print('{position} мeсто: {name} {result}'.format(
                position=position,
                name=k_name,
                result=max(total_registry.get(k_name))
            ))
            total_registry.pop(k_name)
            break


def more_champions(winner, data):
    champions_list = {}
    for k, v in data.items():
        if winner in v:
            champions_list[k] = v[:]
            champions_list[k].remove(winner)
    favorites(champions_list)


def favorites(data):
    max_num_list = [max(value) if len(value) != 0 else 0 for value in data.values()]
    winner = max(max_num_list)
    if max_num_list.count(winner) == 1 and winner != 0:
        one_champion(winner, data)
    elif max_num_list.count(winner) > 1 and winner != 0:
        more_champions(winner, data)
    else:
        print('Невозможно определить победителя на {position} место среди {players}'.format(
            position=position,
            players=tuple(data.keys())
        ))


position = 1
total_registry = {}

total_record = int(input('Сколько записей вносится в протокол? '))
print('Записи: \tРезультат\tИмя')

for num in range(1, total_record + 1):
    check_record(num, total_registry)

if check_players(total_registry, total_record):
    while position != 4:
        favorites(total_registry)
        position += 1
else:
    print('Входных данных недостаточно для формирования списка победителей.')

