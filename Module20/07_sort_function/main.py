def sort(data):
    for num in data:
        if not isinstance(num, int):
            break
    else:
        data = tuple(sorted(list(data)))
    return data


first_tuple = (1, 5, 9, 6, 3, 4, 7)
second_tuple = (1, 5, 9, '6.48', (3, 6), 4, 7)

print(sort(first_tuple))
print(sort(second_tuple))

# TODO: С кортежами со строковыми типами данных тоже должна программа работать корректно
