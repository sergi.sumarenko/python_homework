def is_prime(data):
    new_list = []
    for i_object, obj in enumerate(data):
        if i_object not in (0, 1):
            for num in range(2, i_object):
                if (i_object != num) and (i_object % num == 0):
                    break
            else:
                if isinstance(data, dict):
                    new_list.append(data.get(obj))
                else:
                    new_list.append(obj)
    return new_list


def create_list(data):
    return is_prime(data)


print(create_list([i for i in range(0, 11)]))

# зачтено
