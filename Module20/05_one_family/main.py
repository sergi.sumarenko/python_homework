def search(data, surname):
    surname = surname.lower().capitalize()
    for key, value in data.items():
        if (surname in key) or (surname + 'а' in key) or (surname[:-1] in key):
            for element in key:
                print(element, end=' ')
            print(value)


peoples_info = {
    ('Сидоров', 'Никита'): 35,
    ('Петров', 'Олег'): 55,
    ('Сидорова', 'Алина'): 34,
    ('Сидоров', 'Павел'): 10,
    ('Алдерсон', 'Эллиот'): 22,
    ('Андерсон', 'Томас'): 33
}

user_search = input('Введите фамилию: ')

search(peoples_info, user_search)

# зачтено
