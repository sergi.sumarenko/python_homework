def some_function(data):
    new_dict = {
        interest for val in data.values()
        for interest in val.get('interests')
    }
    lth = 0
    for val in data.values():
        lth += len(val.get('surname'))
    return new_dict, lth


students = {
    1: {
        'name': 'Bob',
        'surname': 'Vazovski',
        'age': 23,
        'interests': ['biology, swimming']
    },
    2: {
        'name': 'Rob',
        'surname': 'Stepanov',
        'age': 24,
        'interests': ['math', 'computer games', 'running']
    },
    3: {
        'name': 'Alexander',
        'surname': 'Krug',
        'age': 22,
        'interests': ['languages', 'health food']
    }
}

for key, value in students.items():
    print('IP студента: {0}, возраст: {1}'.format(key, value.get('age')))

interests, length = some_function(students)

print(
    'Интересы студентов: {0}\n'
    'Общая длина фамилий: {1}'.format(interests, length)
)

# зачтено
