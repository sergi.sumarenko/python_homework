alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

in_file = open('text.txt', 'r')
f = in_file
out_file = open('cipher_text.txt', 'w')

for i, v in enumerate(f):
    cipher_line = ''.join(
        alphabet[alphabet.index(sym) + i + 1]
        if sym in alphabet else sym
        for sym in v
    )
    out_file.write(cipher_line)

out_file.close()
in_file.close()