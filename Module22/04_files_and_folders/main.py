import os


def files_dirs(user_path, dct):
    for obj in os.listdir(user_path):
        new_path = os.path.abspath(os.path.join(user_path, obj))
        if os.path.isdir(new_path):
            dct['quantity_dir'] += 1
            files_dirs(new_path, dct)
        elif os.path.isfile(new_path):
            dct['quantity_file'] += 1
            dct['full_size'] += os.path.getsize(new_path) / 1024


data = {
    'quantity_dir': 0,
    'quantity_file': 0,
    'full_size': 0
}

path = input('Введите абсолютный путь: ')

if os.path.exists(path):
    files_dirs(path, data)
    print(
        'Размер каталога: {full_size} кб'
        '\nКоличество подкаталогов: {quantity_dir}'
        '\nКоличество файлов: {quantity_file}'.format(
            quantity_dir=data.get('quantity_dir'),
            quantity_file=data.get('quantity_file'),
            full_size=round(data.get('full_size'), 2),
        )
    )
else:
    print('Ошибка. Указаный каталог не найден')