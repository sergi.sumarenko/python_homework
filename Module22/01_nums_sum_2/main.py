file = open('numbers.txt', 'r')
summ = 0

for line in file:
    if line != '\n':
        summ += int(line)

file.close()

answer_file = open('answer.txt', 'w')
answer_file.write(str(summ))
answer_file.close()