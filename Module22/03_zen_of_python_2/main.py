import os
path = os.path.join('..', '02_zen_of_python', 'zen.txt')


def work_file(something_function, **kwarg):
    file = open(path, 'r')
    result = something_function(file, **kwarg)
    file.close()
    return result


def count_sym(data):
    symbols = data.read()
    dct = {
        sym: symbols.count(sym)
        for sym in symbols.lower()
        if sym in tuple(
            chr(sym) for sym in range(97, 123)
        )
    }
    return dct


def min_sym(data):
    return sorted(data.items(), key=lambda item: item[1])[0]


def quantity_str(data):
    return len(data.readline())


def count_word(data, lst):
    words = data.read().split()
    word_lst = list()
    for word in words:
        for sym in word:
            if sym.lower() in lst:
                word_lst.append(word)
                break
    return len(word_lst)


sym_dict = work_file(count_sym)
print('Количество символов  латинского алфавита:', len(sym_dict))
print('Реже всего встречается символ:', min_sym(sym_dict))
print('Количество строк:', work_file(quantity_str))
print('Количество слов:', work_file(count_word, lst=sym_dict))
