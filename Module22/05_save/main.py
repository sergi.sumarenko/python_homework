import os

text = input('Введите текс: ')
user_path = input('Куда вы хотите сохранить документ? '
                  'Введите последовательность папок (через пробел): ').split()
path = os.path.join(*user_path)

if os.path.exists(path):
    while True:
        user_file = input('Введите имя файла: ')
        file_path = os.path.join(path, user_file)
        if os.path.exists(file_path):
            question = input('Вы действительно хотите перезаписать файл (да/нет)? ').lower()
            if question == 'да':
                save_file = open(file_path, 'w').write(text)
                print('Файл успешно перезаписан.')
                break
            elif question == 'нет':
                question_two = input('Сохранить в другой файл (да/нет)? ').lower()
                if question_two == 'нет':
                    break
                elif question_two != 'да':
                    print('Ошибка ввода.')
        else:
            save_file = open(file_path, 'w').write(text)
            print('Файл успешно сохранён.')
            break
else:
    print('Ошибка. Указаный путь не найден')



