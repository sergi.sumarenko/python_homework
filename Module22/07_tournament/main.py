in_file = open('first_tour.txt', 'r')
s = in_file.readlines()
new_obj = [('{name}. {surname}'.format(name=line.split()[1][0], surname=line.split()[0]),
            str(line.split()[2])) for line in s[1:] if line.split()[2] >= s[0]]
in_file.close()

out_file = open('second_tour.txt', 'w')
out_file.write(str(len(new_obj)))
for i_elem, elem in enumerate(sorted(new_obj, reverse=True)):
    out_file.write('\n{count}) {name} {ball}'.format(
        count=i_elem + 1,
        name=elem[0],
        ball=elem[1]
    ))
out_file.close()
