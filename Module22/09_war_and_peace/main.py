import zipfile
data_box = zipfile.ZipFile('voyna-i-mir.zip', 'r')
data_file = (data_box.open('voyna-i-mir.txt', 'r')).read()
decode_file = data_file.decode('utf-8')


def analysis(set_symbols, lst_decode):
    new_data = ((sym, lst_decode.count(sym)) for sym in set_symbols)
    return sorted(new_data, key=lambda item: item[1])


sort_list = 'abcdefghijklmnopqrstuvwxyzабвгдеёжзийклмнопрстуфхцчшщъыьэюя'
symbols = {sym for sym in decode_file if sym in (sort_list + sort_list.upper())}

for element in analysis(symbols, decode_file):
    print(element[0], element[1])
data_box.close()
