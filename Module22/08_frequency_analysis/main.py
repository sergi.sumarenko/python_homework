in_file = open('text.txt', 'r').read().lower()
new_obj = {sym: in_file.count(sym) for sym in in_file
           if sym in 'abcdefghijklmnopqrstuvwxyz'}
out_file = open('analysis.txt', 'w')

for key, value in sorted(new_obj.items()):
    out_file.write('{symbol} {frequency}\n'.format(
        symbol=key,
        frequency=round(value / sum(new_obj.values()), 3)
    ))

out_file.close()
