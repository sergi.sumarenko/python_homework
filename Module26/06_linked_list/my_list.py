from typing import Iterable, Union


class LinkedList:
    __count = 0

    def __init__(self, data=None) -> None:
        self.__next = None
        self.__data = data

    def append(self, val: any) -> None:
        """
        Метод добавления обьекта
        :param val: данные привязываемые к ссылке
        """
        LinkedList.__count += 1
        next_obj = LinkedList(val)
        first = self
        while first.__next:
            first = first.__next
        first.__next = next_obj

    def get(self, indx: int):
        """
        Геттер получения данных по индексу
        :param indx: (int) индекс который нужно найти
        :return: данные если такой индекс есть или None если обьект отсутствует
        """
        __lst = self
        for i_obj in range(self.__count):
            __lst = __lst.__next
            if i_obj == indx:
                return __lst.__data

    def remove(self, indx: int) -> None:
        """
        Удалить обьект из списка по индексу
        :param indx: (int) индекс обьекта который нужно удалить
        """
        if 0 < indx >= self.__count:
            print('Обьекта с таким индексом не существует')
        else:
            __lst = self
            if indx == 0:
                __lst.__next = __lst.__next.__next
            else:
                for i_obj in range(self.__count):
                    __lst = __lst.__next
                    if i_obj == indx - 1 or indx == 0:
                        __lst.__next = __lst.__next.__next
                        break
            self.__count -= 1

    def __iter__(self) -> Iterable[Union[any]]:
        self.__count_cycle = self.__count
        self.__first = self
        return self

    def __next__(self) -> Union[any, Exception]:
        if self.__count_cycle != 0:
            self.__count_cycle -= 1
            self.__first = self.__first.__next
            return self.__first.__data
        else:
            raise StopIteration

    def __str__(self) -> str:
        out_str = ''
        lst = self
        for i_obj in range(self.__count):
            lst = lst.__next
            if i_obj == self.__count - 1:
                out_str += str('{}'.format(lst.__data))
            else:
                out_str += str('{} '.format(lst.__data))
        return '[{}]'.format(out_str)