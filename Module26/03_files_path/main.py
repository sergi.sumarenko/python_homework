from typing import TextIO, Iterable, Optional
from os import path, listdir


def gen_files_path(search_dir: str, file: TextIO, start_local='/') -> Optional[Iterable[str]]:
    try:
        for obj in listdir(start_local):
            path_obj = path.join(start_local, obj)
            if obj.split(path.sep)[-1] == search_dir:
                yield path_obj
            elif path.isfile(path_obj):
                file.write('{}\n'.format(path_obj))
            elif path.isdir(path_obj):
                for deep_obj in gen_files_path(search_dir, file, start_local=path_obj):
                    yield deep_obj
    except PermissionError:
        pass


user_search = input('Введите имя искомого файла: ')

with open('files_path.txt', 'w') as path_file:
    for current_path in gen_files_path(user_search, path_file):
        print('Обьект найден по пути {}'.format(current_path))
        break
    else:
        print('Не найден обьект: <<{}>>'.format(user_search))

