from collections.abc import Iterable
from typing import Union


class Iterator:

    def __init__(self, maximum: int) -> None:
        self.__maximum = maximum
        self.__current_num = 0

    def __iter__(self) -> Iterable[int]:
        return self

    def __next__(self) -> Union[int, Exception]:
        if self.__current_num != self.__maximum:
            self.__current_num += 1
            return self.__current_num ** 2
        else:
            raise StopIteration


def generator(num: int) -> Iterable[int]:
    for num in range(1, num + 1):
        yield num ** 2


user_num = int(input('Введите число до которого необходим сформировать обьект: '))

expression = (num ** 2 for num in range(1, user_num + 1))

for number in expression:
    print(number, end=' ')

print()

for number in generator(user_num):
    print(number, end=' ')

print()

my_class = Iterator(user_num)

for number in my_class:
    print(number, end=' ')
