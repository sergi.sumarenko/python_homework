from os import listdir, path
from typing import Iterable, Union


def file_analyses(file_name: str) -> Iterable:
    with open(file_name, 'r') as read_file:
        for line in read_file:
            if line.strip('\n') and not line.startswith('#'):
                yield line


def output(direct: str) -> Union[int, str]:
    main_count = 0
    if path.exists(direct):
        for current_file in listdir(direct):
            if path.isfile(current_file) and current_file.endswith('.py'):
                for _ in file_analyses(current_file):
                    main_count += 1
        return main_count
    else:
        return 'Указаной директории не найдено.'
