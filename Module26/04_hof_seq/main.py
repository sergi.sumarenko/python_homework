from typing import List, Iterable


def hofstadter(deep: int, q_n: List[int]) -> Iterable[int]:
    if q_n != [1, 2]:
        for _ in range(deep):
            result = q_n[-q_n[-1]] + q_n[-q_n[-2]]
            q_n.append(result)
            yield result


test_lst = [1, 1]
for next_int in hofstadter(10, test_lst):
    print(next_int, end=' ')
print()
for next_int in hofstadter(10, test_lst):
    print(next_int, end=' ')
print()
for next_int in hofstadter(10, [1, 2]):
    print(next_int, end=' ')
print()
