from typing import List, Iterable

list_1 = [2, 5, 7, 10]
list_2 = [3, 8, 4, 9]
to_find = 56


def f_result(lst_1: List[int], lst_2: List[int]) -> Iterable[int]:
    for x in lst_1:
        for y in lst_2:
            result = x * y
            print('{} * {} = {}'.format(x, y, result))
            yield result


if to_find in f_result(list_1, list_2):
    print('Found!!!')

