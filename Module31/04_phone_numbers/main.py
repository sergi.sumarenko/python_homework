import re

data = ['9999999999', '999999-999', '99999x9999']

if __name__ == "__main__":
    for index, phone_num in enumerate(data):
        if re.findall(r'[8,9]\d{9}\b', phone_num):
            print(f'{index + 1} номер: всё в порядке')
        else:
            print(f'{index + 1} номер: не подходит')
