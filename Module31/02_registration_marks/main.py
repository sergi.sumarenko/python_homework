import re

data = 'А578ВЕ777 ОР233787 К901МН666 СТ46599 СНИ2929П777 666АМР6'
if __name__ == "__main__":
    print('Список номеров частных автомобилей: {}'.format(
        re.findall(r'\b\w\d{3}\w{2}\d{2,3}\b', data)
    ))

    print('Список номеров такси: {}'.format(
        re.findall(r'\b\w{2}\d{3}\d{2,3}\b', data)
    ))
