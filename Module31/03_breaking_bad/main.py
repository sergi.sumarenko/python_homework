import requests
import json
import re
import collections

RESULT = {
    'ip эпизода': 0,
    'Номер сезона': 0,
    'Номер эпизода': 0,
    'Общее кол-во смертей': 0,
    'Список погибших': list(),
}

if __name__ == "__main__":
    death_api = requests.get('https://breakingbadapi.com/api/deaths')

    season = max(collections.Counter(re.findall(r'"season":\d+\b', death_api.text)))
    episode = max(collections.Counter(re.findall(r'{season},"episode":\d+\b'.format(
        season=season
    ), death_api.text)))

    RESULT['Номер сезона'] = int(re.findall(r'\d+', season)[0])
    RESULT['Номер эпизода'] = int(re.findall(r'\d+', episode)[1])

    for obj in json.loads(death_api.text):
        if (obj.get('season'), obj.get('episode')) == (RESULT.get('Номер сезона'), RESULT.get('Номер эпизода')):
            RESULT.get('Список погибших').append(obj.get('death'))

    RESULT['Общее кол-во смертей'] = len(RESULT.get('Список погибших'))

    episode_api = requests.get('https://breakingbadapi.com/api/episodes')

    for obj in json.loads(episode_api.text):
        if (obj.get('season'), obj.get('episode')) == re.match(r'\d+', episode):
            RESULT['ip эпизода'] = obj.get('episode_id')

    with open('info.json', 'w') as file:
        json.dump(RESULT, file, indent=4, ensure_ascii=False)

    print(json.dumps(RESULT, ensure_ascii=False, indent=4))
