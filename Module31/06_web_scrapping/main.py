import requests
from lxml import html


def search(url: str = 'http://www.columbia.edu/~fdc/sample.html', tag: str = '//h3'):
    data = html.fromstring(requests.get(url).text)

    for h3 in data.xpath(tag):
        print(h3.text)


if __name__ == "__main__":
    search()