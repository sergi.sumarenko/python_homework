class TaskManager:
    __task_list = dict()

    def new_task(self, task, priority):
        if priority not in self.__task_list:
            self.__task_list[priority] = [task]
        else:
            if task not in self.__task_list.get(priority):
                self.__task_list[priority].append(task)
            else:
                print('Задание <<{task}>> с приоритетом <<{priority}>> уже в списке'.format(
                    task=task,
                    priority=priority
                ))

    def delete_task(self, task):
        __del_list = set()
        for key, group_task in self.__task_list.items():
            for i_task in group_task:
                if task in i_task:
                    group_task.remove(i_task)
            if not self.__task_list.get(key):
                __del_list.add(key)
        for num in __del_list:
            self.__task_list.pop(num)

    def __sorted(self):
        __output = ''
        __sorted_list = sorted(self.__task_list.copy().items(), key=lambda k: k[0])
        for key, value in __sorted_list:
            __output += '{key}: {sort_value}\n'.format(
                key=key,
                sort_value='; '.join(''.join(elem) for elem in reversed(value)))
        return __output

    def __str__(self):
        return self.__sorted()


manager = TaskManager()
manager.new_task('сделать уборку', 4)
manager.new_task('помыть посуду', 4)
manager.new_task('отдохнуть', 1)
manager.new_task('поесть', 2)
manager.new_task('поесть', 2)
print(manager)
manager.new_task('поесть', 3)
manager.new_task('сделать дз', 2)
manager.delete_task('поесть')
print(manager)
