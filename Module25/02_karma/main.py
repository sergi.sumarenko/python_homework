from random import randint, choice


class LiveExceptions(Exception):
    __name = 'LiveExceptions'
    __info = 'Что-то о LiveExceptions'

    def get_name(self):
        return self.__name

    def get_info(self):
        return self.__info


class KillError(LiveExceptions):
    _LiveExceptions__name = 'KillError'
    _LiveExceptions__info = 'Что-то о KillError'


class DrunkError(LiveExceptions):
    _LiveExceptions__name = 'DrunkError'
    _LiveExceptions__info = 'Что-то о DrunkError'


class CarCrashError(LiveExceptions):
    _LiveExceptions__name = 'CarCrashError'
    _LiveExceptions__info = 'Что-то о CarCrashError'


class GluttonyError(LiveExceptions):
    _LiveExceptions__name = 'GluttonyError'
    _LiveExceptions__info = 'Что-то о GluttonyError'


class DepressionError(LiveExceptions):
    _LiveExceptions__name = 'DepressionError'
    _LiveExceptions__info = 'Что-то о DepressionError'


class Live:
    __current_const = 0
    __count = 1
    __all_live_error = (KillError, DrunkError, CarCrashError, GluttonyError, DepressionError)

    def __init__(self):
        with open('karma.log', 'w') as karma_file:
            while self.__current_const < 500:
                try:
                    if randint(1, 10) == 5:
                        __current_error = choice(self.__all_live_error)()
                        raise __current_error
                    else:
                        self.__current_const += randint(1, 7)
                except self.__all_live_error:
                    karma_file.write('День {day}:\n\t{name}: {value}\n'.format(
                        day=str(self.__count),
                        name=__current_error.get_name(),
                        value=__current_error.get_info()
                    ))
                finally:
                    self.__count += 1


Live()
