class Person:

    def __init__(self, name, surname, age):
        self.__set_name(name)
        self.__set_surname(surname)
        self.__set_age(age)

    def __set_name(self, value):
        self.__name = value

    def __set_surname(self, value):
        self.__surname = value

    def __set_age(self, value):
        self.__age = value

    def get_name(self):
        return self.__name

    def get_surname(self):
        return self.__surname

    def get_age(self):
        return self.__age


class Employee(Person):
    position = None

    def __init__(self, name, surname, age):
        super().__init__(name, surname, age)
        self.salary()

    def salary(self, *args):
        pass

    def get_salary(self):
        return self.salary()

    def get_position(self):
        return self.position


class Manager(Employee):
    __salary = 13000

    def __init__(self, name, surname, age):
        super().__init__(name, surname, age)
        self.position = 'Менеджер'

    def salary(self):
        return self.__salary


class Agent(Employee):
    __salary = 5000
    __sales_volume = 0

    def __init__(self, name, surname, age, sales_volume):
        super().__init__(name, surname, age)
        self.__sales_volume = sales_volume
        self.position = 'Агент'

    def salary(self):
        return self.__salary + self.__sales_volume * 0.05


class Worker(Employee):
    __salary = 100
    __quantity_hours_worked = 0

    def __init__(self, name, surname, age, quantity_hours_worked):
        super().__init__(name, surname, age)
        self.__quantity_hours_worked = quantity_hours_worked
        self.position = 'Работник'

    def salary(self):
        return self.__salary * self.__quantity_hours_worked
