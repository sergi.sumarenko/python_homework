from accounting import Agent, Manager, Worker
from random import randint

data = (
    Manager('Александр', 'Иванов', 25),
    Manager('Алексей', 'Смирнов', 32),
    Manager('Андрей', 'Кузнецов', 45),
    Agent('Артем', 'Попов', 35, randint(50000, 500000)),
    Agent('Виктор', 'Васильев', 24, randint(50000, 500000)),
    Agent('Даниил', 'Петров', 22, randint(50000, 500000)),
    Worker('Дмитрий', 'Соколов', 23, randint(120, 275)),
    Worker('Егор', 'Михайлов', 45, randint(120, 275)),
    Worker('Илья', 'Иванов', 44, randint(120, 275))
)

print(('+' + '-' * 15 + '+') * 4)
print('|{: ^15}||{: ^15}||{: ^15}||{: ^15}|'.format(
    'Имя', 'Фамилия', 'Должность', 'Зарплата'
))
for human in data:
    print(('+' + '-' * 15 + '+') * 4)
    print('|{: ^15}||{: ^15}||{: ^15}||{: ^15.2f}|'.format(
        human.get_name(),
        human.get_surname(),
        human.get_position(),
        human.get_salary()
    ))
print(('+' + '-' * 15 + '+') * 4)

