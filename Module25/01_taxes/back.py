import sys
from front import Ui_MainWindow
from PyQt5 import QtCore, QtWidgets
all_result = dict()


class Property:
    """
    Домашние задание
    """
    __tax = 0

    def __init__(self, worth):
        self.worth = worth

    def tax(self, denominator):
        try:
            self.__tax = float(self.worth) / denominator
        except (AttributeError, ValueError, TypeError):
            pass

    def get_tax(self):
        return round(self.__tax, 2)




class Appartment(Property):
    """
    Домашние задание
    """
    __denominator = 1000

    def __init__(self, worth):
        super().__init__(worth)
        self.tax(self.__denominator)


class Car(Property):
    """
    Домашние задание
    """
    __denominator = 200

    def __init__(self, worth):
        super().__init__(worth)
        self.tax(self.__denominator)


class CountryHouse(Property):
    """
    Домашние задание
    """
    __denominator = 500

    def __init__(self, worth):
        super().__init__(worth)
        self.tax(self.__denominator)


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):

    """
    Класс создания и обработки событий в GUI

    """

    def __init__(self, *args, **kwargs):
        """
        Взаимодействия с пользователем, ожидание ввода и/или нажание checkbox, __button
        :param args: обьекты GUI
        :param kwargs: обьекты GUI
        """
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setupUi(self)
        self.__checked(self.CheckHome)
        self.__checked(self.CheckCar)
        self.__checked(self.CheckHouse)
        self.__button()

    def __checked(self, checkbox):
        """
        Метод ждет изменения состояния от пользователя
        :param checkbox: Обьект GUI
        :return: при изменении флага вызывает функцию __new_obj
        """
        checkbox.stateChanged.connect(
            self.__new_obj
        )

    def __new_obj(self):
        """
        Метод посредник для создания полей ввода в функции __checked
        :return: вызывает функцию __bit_new_obj
        """
        self.__bit_new_obj(self.CheckHome, self.HomeCost, 140)
        self.__bit_new_obj(self.CheckCar, self.CarCost, 180)
        self.__bit_new_obj(self.CheckHouse, self.HouseCost, 220)

    def __bit_new_obj(self, checkbox, obj, position):
        """
        Проверяет флаг определённого чекбокса
        :param checkbox: какой GUI checkbox проверяем (наличие дома, машини, квартиры)
        :param obj: какому обьекту устанавливаем новые параметры (поля для ввода стоимости)
        :param position: позиция поля ввода
        :return: устанавливает style параметры полю ввода для возможности внесения данных пользователем
        """
        if checkbox.isChecked():
            obj.setGeometry(QtCore.QRect(140, position, 181, 27))
        else:
            obj.setGeometry(QtCore.QRect(0, 0, 0, 0))

    def __button(self):
        """
        Метод ожидания нажатия кнопки вывода результатов
        :return: вызывает метод result
        """
        self.pushButton.clicked.connect(self.result)

    def result(self):
        """
        Метод для получения внесёных пользовательских данных
        :return: добавляет данные из поля name в глобальную переменную dict - result,
        вызывает методы check_result
        """
        if self.LineForName.text():
            all_result['name'] = self.LineForName.text()
        else:
            all_result['name'] = 'Аноним'

        self.check_result(self.CheckHome, 'Квартира', self.HomeCost)
        self.check_result(self.CheckHouse, 'Дом', self.HouseCost)
        self.check_result(self.CheckCar, 'Машина', self.CarCost)

    def check_result(self, flag_name, name_obj, obj_value):
        """
        Метод получения данных / вывода рультата
        :param flag_name: проверяет необходимость расчета для опреденного обьекта (дам, машина, квартира)
        :param name_obj: key для all_result
        :param obj_value: value для all_result
        :return: выводит пользователю результат обработки данных программой
        """
        if flag_name.isChecked():
            all_result.__setitem__(name_obj, obj_value.text())
            self.Output.setText('{}, вам нужно заплатить:\n\n{}'.format(
                all_result.get('name').capitalize(),
                Result().get_out()
            ))
        else:
            all_result.__setitem__(name_obj, 'отсутствует')


class Result:
    """
    Класс обработки данных
    """
    __out = dict()

    def __init__(self):
        """
        При инициализация класса Result формирует словарь с обработаными данными согласно условию задачи
        """
        self.__out.__setitem__('Квартира', Appartment(all_result.get('Квартира', 'Отсутсвует')).get_tax())
        self.__out.__setitem__('Машина', Car(all_result.get('Машина', 'Отсутсвует')).get_tax())
        self.__out.__setitem__('Дача', CountryHouse(all_result.get('Дом', 'Отсутсвует')).get_tax())

    def get_out(self):
        """
        :return (str): результат обработки введёных пользователем данных
        """
        return '\n\n'.join(
            '\t{}: {}$'.format(k, v) for k, v in self.__out.items() if v != 0.0
        )


class Start(MainWindow):
    """
    Класс для запуска GUI
    """
    app = QtWidgets.QApplication(sys.argv)

    window = MainWindow()
    window.show()
    app.exec()



