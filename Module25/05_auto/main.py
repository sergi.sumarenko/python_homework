import math


class Car:
    __current_direction = None

    def __init__(self, x, y, direction):
        self.__current_x = x
        self.__current_y = y
        self.set_direction(math.radians(direction))

    def move(self, distance):
        self.__current_x = round(distance * math.cos(self.__current_direction), 2)
        self.__current_y = round(distance * math.sin(self.__current_direction), 2)

    def set_direction(self, angle):
        self.__current_direction = angle


class Bus(Car):
    __passenger = 0
    __money = 0

    def __init__(self, x, y, direction):
        super().__init__(x, y, direction)

    def in_bus(self, quantity):
        self.__passenger += quantity
        self.__money += self.__passenger * 5

    def out_bus(self, quantity):
        self.__passenger -= quantity

    def move(self, distance):
        super().move(distance)
        self.__money -= 2 * distance

    def get_passenger(self):
        return self.__passenger

    def get_money(self):
        return round(self.__money, 2)
