class Nightstand:
    """
    Класс тумбочка, хранящая запас денег в доме
    """
    __money = 100

    def add_money(self):
        """
        Добавляет к имеющимся деньгам в тумбочке заработаные мужем деньги
        """
        self.__money += 150

    def take_money(self, quantity):
        """
        :param quantity: type (int) изымаемое кол-во денег из тумбочки
        """
        self.__money -= quantity

    def get_money(self):
        """
        Доступ к значению общего кол-ва денег
        :return: текущий баланс
        :type (int)
        """
        return self.__money


class Fridge:
    """
    Холодильник, класс для хранения, а так же операций изьятия и добавления еды в доме
    """
    __human_food = 50
    __cat_food = 30

    def add_h_food(self, quantity):
        """
        Добавляем еду для людей
        :param quantity: кол-во еды
        :type quantity: int
        """
        self.__human_food += quantity

    def take_h_food(self, quantity):
        """
        Забираем еду для людей
        :param quantity: кол-во еды
        :type quantity: int
        """
        self.__human_food -= quantity

    def add_c_food(self, quantity):
        """
        Добавляем еду для котов
        :param quantity: кол-во еды
        :type quantity: int
        """
        self.__cat_food += quantity

    def take_c_food(self, quantity):
        """
        Забираем еду для котов
        :param quantity: кол-во еды
        :type quantity: int
        """
        self.__cat_food -= quantity

    def get_h_food(self):
        """
        Геттер для значения человеской еды
        :return: __human_food
        :type: int
        """
        return self.__human_food

    def get_c_food(self):
        """
        Геттер для значения еды котов
        :return: __cat_food
        :type: int
        """
        return self.__cat_food


class Dirt:
    """
    Класс для грязи в доме
    """
    __dirt = 0

    def add_dirt(self, quantity):
        """
        Добавить грязь в дом
        :param quantity: кол-во грязи
        :type quantity: int
        """
        self.__dirt += quantity

    def clean_dirt(self, quantity):
        """
        Убрать грязь из дома, так же корректирует максимальное значение убраной грязи за раз до 100 (условие д/з)
        :param quantity: кол-во убираемой грязи
        :type quantity: int
        """
        if quantity > 100:
            quantity = 100
        self.__dirt -= quantity

    def get_dirt(self):
        """
        Геттер для значения грязи в доме
        :return: __dirt
        :type: int
        """
        return self.__dirt


class House(Nightstand, Fridge, Dirt):
    """
    Класс дом для взаимодействия с жильцами
    """
    __all_h_food = 0
    __all_c_food = 0
    __all_money = 0
    __fur_coat = 0
    __lodging_list = set()

    def coat_add(self):
        """
        Прибавить купленую шубу к общему кол-ву шуб: __fur_coat
        """
        self.__fur_coat += 1

    def money_add(self):
        """
        Прибавить заработаные деньги к общему кол-ву денег: __all_money
        """
        self.__all_money += 150

    def h_food_add(self, h_food):
        """
        Прибавить сьеденые продукты людьми к общему кол-ву сьеденого людьми: __all_h_food
        """
        self.__all_h_food += h_food

    def c_food_add(self, c_food):
        """
        Прибавить сьеденые продукты котами к общему кол-ву сьеденого котами: __all_c_food
        """
        self.__all_c_food += c_food

    def __str__(self):
        """
        Статистика по проживанию согласно условию задания
        :return: Статистика по всем сьеденым продуктам котами и людьми, заработаным деньгам и купленым шубам
        :type (str)
        """
        return 'Заработано денег {money}\nСьедено еды:\n\tЛюди: {h_food}\n\tЖивотные: {c_food}\n' \
               'Куплено шуб: {coat}'.format(
                money=self.__all_money,
                h_food=self.__all_h_food,
                c_food=self.__all_c_food,
                coat=self.__fur_coat
                )

    def lodging(self, who):
        """
        Добавляет в список жильцов дома
        :param who: созданый обитатель дома
        :type: who: class
        """
        self.__lodging_list.add(who)

    def get_list(self):
        """
        Получение списка жильцов
        :return: __lodging_list - список жильцов
        :type: __lodging_list: set
        """
        return self.__lodging_list

