import family

house = family.House()
husband = family.Husband(name='Jack', house=house)
wife = family.Wife(name='Alisa', house=house)
child = family.Child(name='Pups', house=house, mother=wife, father=husband)
first_cat = family.Cat(name='Kukarachi', house=house)
second_cat = family.Cat(name='Tom', house=house)
third_cat = family.Cat(name='Oggy', house=house)

for day in range(1, 365):
    house.add_dirt(5)
    for obj in house.get_list():
        if obj.alive:
            obj.survive()

    if house.get_dirt() > 85:
        for human in (husband, wife, child):
            human.down_happy(10)

for obj in house.get_list():
    print('Житель {}. Обьект наблюдения дожил до конца эксперемента: {}'.format(
        obj.get_name(),
        obj.alive
    ))
print(house)
print('Денег в тумбочке к концу экспиримента: {:.2f}'.format(house.get_money()))
