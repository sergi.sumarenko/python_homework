from house import House
from random import choice


class Satiety:
    """
    Класс сытости

    __current_satiety (int): текущие кол-во сытости, на старте 30
    """
    __current_satiety = 30

    def up_satiety(self, quantity_food, multi=1):
        """
        Добавить сытость в зависимости от сьеденой еды
        :param quantity_food: кол-во еды
        :param multi: множитель с учетом того кто есть. Для человека остаётся стандартный, для кота равен 2
        :type: quantity_food: int
        """
        self.__current_satiety += quantity_food * multi

    def down_satiety(self):
        """
        Отнять сытость
        """
        self.__current_satiety -= 10

    def get_satiety(self):
        """
        Геттер параметра сытости
        :return: __current_satiety
        :type (int)
        """
        return self.__current_satiety


class Happy:
    """
    Класс счастья

     __current_happy (int): текущие кол-во счастья, на старте 100
    """
    __current_happy = 100

    def up_happy(self, quantity):
        """
        Прибавить счастья
        :param quantity: кол-во прибавляемого счастья
        """
        self.__current_happy += quantity

    def down_happy(self, quantity):
        """
        Отнять счастье
        :param quantity: кол-во отнимаемого счастья
        """
        self.__current_happy -= quantity

    def get_happy(self):
        """
        Геттер параметра счастья
        :return: __current_happy
        :type (int)
        """
        return self.__current_happy


class Alive(Satiety, Happy):
    """
    Класс общих методов для обьектов в доме

    :arg: name (str): имя обьекта
    :arg: house (class): дом к тоторому привязан обьект

    Attributes:
        alive (bool): состояние живой или мёртвый обьект
    """
    alive = True

    def __init__(self, name, house):
        self.__set_name(name)
        self.house = house
        self.house.lodging(self)

    def __set_name(self, name):
        """
        Установка имени
        :param name: Имя обьекта
        :type (str)
        """
        self.__name = name

    def sleep(self):
        """
        Метод сон используют коты(Cat) и дети(Child)
        """
        self.down_satiety()

    def get_name(self):
        """
        Получить имя обьекта
        :return: __name
        """
        return self.__name

    def check_state(self):
        """
        Проверка может ли обьект дальше существовать согласно условию
        :return: изменяет состояние обкта с живого на мертвого, если выполнены условия что сытость меньше 0,
        а счастье меньше 10
        """
        if self.get_satiety() < 0 or self.get_happy() < 10:
            self.alive = False


class Human(House, Alive):
    """
    Класс для общих методов людей
    """

    def pet_cat(self):
        """
        Погладить кота - увеличивает счастье, но уменьшает сытость
        """
        self.up_happy(5)
        self.down_satiety()

    def to_eat(self):
        """
        Метод поесть, проверяет наличие еды в холодильнике

        Attributes:
            need_eat (int): определяет сколько еды нужно сьесть с учетом текущего состояния голода. В решение число
            30 взято за основу как максимально допустимое значение сытости
        :return: False в случае если еды не обнаружено
        """
        if self.house.get_h_food() > 0:
            need_eat = 30 - self.get_satiety()
            if need_eat > self.house.get_h_food():
                need_eat = self.house.get_h_food()
            self.up_satiety(need_eat)
            self.house.take_h_food(need_eat)
            self.house.h_food_add(h_food=int(need_eat))
        else:
            return False


class Husband(Human):
    """
    Класс муж

    Args:
        name (str): имя мужа
        house (class): привязка к дому
    """

    def __init__(self, name, house):
        super().__init__(name, house)

    def play(self):
        """
        Игра - увеличивает счастье на 20, но уменьшает сытость
        """
        self.up_happy(20)
        self.down_satiety()

    def go_work(self):
        """
        Работа - увеличивает кол-во денег в доме гна 150, но уменьшает сытость
        """
        self.house.add_money()
        self.house.money_add()
        self.down_satiety()

    def all_functions(self):
        """
        Список всех методов обьекта Husband, согласно условию
        Необходим для рандома
        :return: tuple(to_eat, go_work, play, pet_cat)
        """
        return self.to_eat, self.go_work, self.play, self.pet_cat

    def survive(self):
        """
        Способ дожить по умолчанию (иногда работает =)) ), при желании можно написать свой
        """
        if self.house.get_money() < 50:
            self.go_work()
        elif self.get_satiety() <= 10:
            self.to_eat()
        elif self.get_happy() <= 15:
            choice((self.play, self.pet_cat))()
        else:
            choice(self.all_functions())()
        self.check_state()


class Wife(Human):
    """
    Класс жена

    Args:
        name (str): имя жены
        house (class): привязка к дому
    """
    def __init__(self, name, house):
        super().__init__(name, house)

    def shopping(self):
        """
        Метод покупок

        need_h_food (int): сколько еды нехватает в холодильнике для людей. В решении использовано число 50 как
        максимально допустимое кол-во еды в холодильнике для людей
        need_c_food (int): сколько еды нехватает в холодильнике для котов. В решении использовано число 30 как
        максимально допустимое кол-во еды в холодильнике для котов

        В случае нехватки денег для покупки всех необходимых продуктов need_h_food + need_c_food, покумаем продукты
        с учётом кол-ва имеющихся денег, и процентного соотношея необходимых продуктов
        """
        need_h_food = 50 - self.house.get_h_food()
        need_c_food = 30 - self.house.get_c_food()
        if self.house.get_money() > 0:
            if self.house.get_money() >= need_h_food + need_c_food:
                self.for_shopping(need_h_food, need_c_food)
            else:
                can_buy_h_food = self.house.get_money() * (need_h_food / (need_h_food + need_c_food))
                can_buy_c_food = self.house.get_money() * (need_c_food / (need_h_food + need_c_food))
                self.for_shopping(can_buy_h_food, can_buy_c_food)
        self.down_satiety()

    def for_shopping(self, value_h, value_c):
        """
        для уменьшения кода в методе shopping
        :param value_h: кол-во еды покупаемое для людей
        :param value_c: кол-во еды покупаемое для котов
        """
        self.house.add_h_food(value_h)
        self.house.add_c_food(value_c)
        self.house.take_money(value_h + value_c)

    def buy_fur_coat(self):
        """
        Покупка шубы
        Проверяем наличие денег на шубу и в случае если денег хватает покупаем её увеличивая счастье жены,
        но уменьшая капитал на 350 и её сытость
        """
        if self.house.get_money() >= 350:
            self.up_happy(60)
            self.house.coat_add()
            self.house.take_money(350)
            self.down_satiety()

    def clean_house(self):
        """
        Уборка дома - уменьшает кол-во грязи в доме, не больше чем на 100, уменьшает сытость
        """
        self.house.clean_dirt(self.house.get_dirt())
        self.down_satiety()

    def all_functions(self):
        """
        Список всех методов обьекта Husband, согласно условию
        Необходим для рандома
        :return: tuple(to_eat, shopping, buy_fur_coat, clean_house, pet_cat)
        """
        return self.to_eat, self.shopping, self.buy_fur_coat, self.clean_house, self.pet_cat

    def survive(self):
        """
        Способ дожить по умолчанию (иногда работает =)) ), при желании можно написать свой
        """
        if self.get_satiety() <= 10:
            if not self.to_eat():
                self.shopping()
        elif self.house.get_c_food() < 5 or self.house.get_h_food() < 20:
            self.shopping()
        elif self.house.get_dirt() > 90:
            self.clean_house()
        elif self.get_happy() <= 15:
            if self.house.get_money() < 350:
                choice((self.buy_fur_coat, self.pet_cat))()
            else:
                self.pet_cat()
        else:
            choice(self.all_functions())()
        self.check_state()


class Cat(Alive):
    """
    Класс кот
    """

    def wallpaper(self):
        """
        Драть обои - увеличивает грязь в доме на 5. уменьшает сытость
        """
        self.house.add_dirt(5)
        self.down_satiety()

    def to_eat(self):
        """
        Метод поесть для кота - проверяет наличие еды с учетом того,
        что за 1 сьеденую единицу еды кот получит 2 к сытости
        :return: метод драть обои(wallpaper) и False, если еды не будет
        """
        if self.house.get_c_food() != 0:
            need_eat = (30 - self.get_satiety()) // 2
            if need_eat > self.house.get_c_food():
                need_eat = self.house.get_c_food()
            self.up_satiety(need_eat, 2)
            self.house.take_c_food(need_eat)
            self.house.c_food_add(c_food=int(need_eat))
        else:
            self.wallpaper()
            return False

    def check_state(self):
        """
        Полиморфизм для check_state в классе Alive,
        так как кот всегда счаслив у него этот параметр не учитывается при расчёте состояния жизни/смерти
        """
        if self.get_satiety() < 0:
            self.alive = False

    def all_functions(self):
        """
        Список всех методов обьекта Husband, согласно условию
        Необходим для рандома
        :return: tuple(to_eat, sleep, wallpaper)
        """
        return self.to_eat, self.sleep, self.wallpaper

    def survive(self):
        """
        Способ дожить по умолчанию (иногда работает =)) ), при желании можно написать свой
        """
        if self.get_satiety() <= 10:
            self.to_eat()
        else:
            choice(self.all_functions())()
        self.check_state()


class Child(Human):
    """
    Класс ребёнок

    Args:
        name (str): имя мужа
        house (class): привязка к дому
        mother (class): привязка к матери
        father (class): привязка к отцу
    """
    def __init__(self, name, house, mother, father):
        super().__init__(name, house)
        self.__mother = mother
        self.__father = father

    def destroy(self):
        """
        Уничтожить что-то в доме - уменьшает капитал семьи на 50, увеличивает грязь в доме на 20,
        увеличивает счастье ребёнка на 15, уменьшает сытость
        """
        self.house.take_money(50)
        self.house.add_dirt(20)
        self.up_happy(15)
        self.down_satiety()

    def give_happy(self):
        """
        Дарить счастье - увеличивает счастье родителей на 15, своё на 5, уменьшает сытость
        :return:
        """
        self.__mother.up_happy(15)
        self.__father.up_happy(15)
        self.up_happy(5)
        self.down_satiety()

    def all_functions(self):
        """
        Список всех методов обьекта Husband, согласно условию
        Необходим для рандома
        :return: tuple(to_eat, sleep, give_happy, destroy)
        """
        return self.to_eat, self.sleep, self.give_happy, self.destroy

    def survive(self):
        """
        Способ дожить по умолчанию (иногда работает =)) ), при желании можно написать свой
        """
        if self.get_satiety() <= 10:
            self.to_eat()
        else:
            choice(self.all_functions())()
        self.check_state()
