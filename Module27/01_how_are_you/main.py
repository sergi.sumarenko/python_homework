from typing import Callable, Any
import functools


def how_are_you(func: Callable) -> Callable:
    """
    Декоратор шутка, для любой функции
    :param func: функция которую нужно выполнить
    :return: результат выполнения wrapper_func
    """
    @functools.wraps(func)
    def wrapper_func(*args, **kwargs) -> Any:
        input('Как дела? ')
        print('А у меня не очень!')
        return func(*args, **kwargs)

    return wrapper_func


@how_are_you
def something_func() -> Any:
    """
    Любая функция
    """
    print('В функции <<{name}>> что-то происходит'.format(
        name=something_func.__name__
    ))


something_func()

