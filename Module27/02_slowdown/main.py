from time import sleep
from typing import Callable, Any
import functools


def sleeping(func: Callable) -> Callable:
    """
    Декоратор, счетчик перед стартом функции
    :param func: функция которую нужно выполнить
    :return: результат выполнения wrapper_func
    """
    @functools.wraps(func)
    def wrapper_func(*args: Any, **kwargs: Any) -> Any:
        for sec in range(3, 0, -1):
            print('До старта {}...'.format(sec))
            sleep(0.99)
        print('Старт!!!')
        return func(*args, **kwargs)

    return wrapper_func


@sleeping
def something_func() -> Any:
    """
    Любая функция
    """
    print('В функции <<{name}>> что-то происходит'.format(
        name=something_func.__name__
    ))


something_func()
