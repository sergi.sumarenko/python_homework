from typing import Callable, Any
import functools


def debug(func: Callable) -> Callable:
    """
    Декоратор, обработчик ошибок
    :param func: функция которую нужно выполнить
    :return: результат выполнения wrapper_func:
    """
    @functools.wraps(func)
    def wrapper_func(*args, **kwargs) -> Any:
        print('Вызывается {func_name}({args_kwargs})'.format(
            func_name=func.__name__,
            args_kwargs=', '.join(
                (', '.join(args),
                 ', '.join('='.join((key, str(val))) for key, val in kwargs.items()))
                if len(kwargs) != 0 else args
            )
        ))
        result = func(*args, **kwargs)
        print('"{}" вернула значение: {}'.format(func.__name__, result))
        if isinstance(kwargs.get('age'), int) and kwargs.get('age') >= 100:
            print('Ого, {func_arg_name}! Тебе уже {func_kwarg_age} лет, ты вырос!'.format(
                func_arg_name=args[0], func_kwarg_age=kwargs.get('age')
            ))
        print()
    return wrapper_func


@debug
def greeting(name: str, age: int = None) -> str:
    if age:
        return 'Ого, {name}! Тебе уже {age} лет, ты быстро растёшь!'.format(name=name, age=age)
    else:
        return 'привет, {name}!'.format(name=name)


greeting('Tom')
greeting('Misha', age=100)
greeting('Dom', age=16)

