from typing import Any, Callable
import functools


class Count:
    """
    Класс счетчик
    __quantity_dict (dict) - словарь для вызываемых функций
    """
    __quantity_dict = dict()

    def __init__(self, func_name: str) -> None:
        """
        При обращении к классу создает или добавляет значение в словарь '__quantity_dict' кол-ва вызовов функции,
        выводит  информацию о количестве вызовов определённой функцией
        :param func_name: имя функции за которой ведётся подсчет
        """
        if func_name in self.__quantity_dict:
            Count.__quantity_dict[func_name] += 1
        else:
            self.__quantity_dict[func_name] = 1
        print('Функция: {func_name} вызвана уже целых: {count} раз(а)'.format(
            func_name=func_name, count=self.__quantity_dict.get(func_name)
        ))


def counter(func) -> Callable:

    @functools.wraps(func)
    def wrapper_func(*args: Any, **kwargs: Any) -> Any:
        Count(func.__name__)
        return func(*args, **kwargs)

    return wrapper_func


@counter
def something_func() -> Any:
    """
    Любая функция
    """
    print('В функции <<{name}>> что-то происходит\n'.format(
        name=something_func.__name__
    ))


@counter
def something_func2() -> Any:
    """
    Любая функция
    """
    print('В функции <<{name}>> что-то происходит\n'.format(
        name=something_func2.__name__
    ))


something_func()
something_func2()
something_func()
something_func2()
something_func()
something_func2()
something_func()