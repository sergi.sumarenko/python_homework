from typing import Callable, Any
import functools, datetime, sys


def logging(func: Callable) -> Callable:
    """
    Декоратор, обработчик ошибок
    :param func: функция которую нужно выполнить
    :return: результат выполнения wrapper_func
    """
    @functools.wraps(func)
    def wrapper_func(*args: Any, **kwargs: Any) -> Any:
        """
        Обвёртка для выполняемой func
        :param args: любые позиционные аргументы для декорируемой функции
        :param kwargs: любые именованые аргументы для декорируемой функции
        :return: результат декорируемой функции или запись в log_file, при возникновении ошибки
        """
        try:
            return func(*args, **kwargs)
        except:
            with open('function_errors.log', 'a') as log_file:
                log_file.write('{time_date}:\n\t{func}:\n\t\t{error_name}: {error_info}\n'.format(
                    time_date=str(datetime.datetime.now()).split('.')[0], func=func.__name__,
                    error_name=sys.exc_info()[0].__name__, error_info=sys.exc_info()[1]
                ))

    return wrapper_func


@logging
def something_func() -> Any:
    """
    Любая функция
    """
    print('В функции <<{name}>> что-то происходит'.format(
        name=something_func.__name__
    ))
@logging
def something_func2() -> Any:
    """
    Любая функция
    """
    print('В функции <<{name}>> что-то происходит'.format(
        name=something_func.__name__
    ))


something_func(132)
something_func2(123)
