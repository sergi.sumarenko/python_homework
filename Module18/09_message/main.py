alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'

text = input('Введите текст: ')

count = 0
new_str = []

for i_sym in range(len(text)):
    if text[i_sym].lower() in alphabet:
        new_str.insert(i_sym - count, text[i_sym])
        count += 1
    else:
        new_str.append(text[i_sym])
        count = 0

print(''.join(new_str))
