def num_check():
    check = []
    count_inx = 1
    for sym in ip_list:
        if not sym.isdigit() or (int(sym) > 255) or (int(sym) < 0):
            check.append('{0}: {1}'.format(count_inx, sym))
        count_inx += 1
    return '; '.join(check)


while True:
    ip_string = input('Введите ip address: ')
    ip_list = [
        ('{} символ не указан'.format(sym) if sym == '' else sym)
        for sym in ip_string.split('.')
    ]
    if ip_string.count('.') != 3:
        print('Ошибка. неверное количество разделителей "."')
    elif num_check():
        print('Ошибка введёных номеров в ip address: {}'.format(num_check()))
    else:
        print("ip address ok")
        break
