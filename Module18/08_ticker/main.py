first_str = input('Введите первую строку: ')
second_str = input('Введите вторую строку: ')
count = 0

if len(first_str) != len(second_str):
    print('Первую строку нельзя получить из второй, из за разной длинны строк.')
else:
    for _ in range(len(first_str)):
        if first_str == second_str:
            print('Первая строка получается из второй со сдвигом {}.'.format(count))
            break
        else:
            count += 1
            second_str = ''.join([second_str[-1], second_str[:-1]])
    else:
        print('Первую строку нельзя получить из второй с помощью циклического сдвига.')
