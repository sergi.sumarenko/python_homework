while True:
    count_digit = 0
    count_tsym = 0
    password = input('Введите пароль: ')
    for sym in password:
        if sym.isupper():
            count_tsym += 1
        elif sym.isdigit():
            count_digit += 1
    if count_digit >= 3 and count_tsym >= 1:
        print('Это надёжный пароль!')
        break
    print('Пароль ненадёжный. Попробуйте ещё раз.')
