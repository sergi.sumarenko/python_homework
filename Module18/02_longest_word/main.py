text = input('Введите текст: ').split()
words_len = [len(word) for word in text]

print('Самое длинное слово: {0}, его длинна {1} символов'.format(text[words_len.index(max(words_len))],
                                                                 max(words_len)))
