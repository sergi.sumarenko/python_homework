forbidden_symbols = '@', '№', '$', '%', '^', '&', '*', '(', ')'

file_name = input('Введите полное имя файла: ')

if file_name.startswith(forbidden_symbols):
    print('Название файла не может начинаться на {}'.format(forbidden_symbols))
elif not file_name.endswith(('.txt', '.docx')):
    print('Расширение файла может быть: .txt или .docx')
else:
    print('Имя файла отвечает всем требованиям.')
