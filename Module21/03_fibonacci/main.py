def new_test(search, out=1, interim=0):
    if search == 0:
        return out
    return new_test(search - 1, out + interim, interim=out)


user_num = int(input('Введите позицию числа в ряде Фибаначчи: '))
print(new_test(user_num - 1))
