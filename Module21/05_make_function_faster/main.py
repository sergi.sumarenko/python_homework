def calculating_math_func(num, data=None):
    if num in data.keys():
        result = data[num]
    else:
        result = data[max(data.keys())]
        for index in range(max(data.keys()) + 1, num + 1):
            result *= index
            data[index] = result
    result /= num ** 3
    result = result ** 10
    return result


factorial_data = {1: 1}
while True:
    user_num = int(input('Введите число (0 для выхода): '))
    if not user_num:
        break
    print(calculating_math_func(user_num, factorial_data))
