def my_zip(first, second):
    ans = ((first[i], second[i]) for i in range(min(len(first), len(second))))
    return ans


string = 'abcd'
num_tuple = (10, 20, 30)
for elem in my_zip(string, num_tuple):
    print(elem)
