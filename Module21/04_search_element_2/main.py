site = {
    'html': {
        'head': {
            'title': 'Мой сайт'
        },
        'body': {
            'h2': 'Здесь будет мой заголовок',
            'div': 'Тут, наверное, какой-то блок',
            'p': 'А вот здесь новый абзац',
        },
        'footer': {
            'container': {
                'div': 'Ещё один блок, только поглубже'
            }
        }
    }
}


def search(data, search_object, depth=None, lst=None):
    if depth == 0:
        return False
    elif search_object in data:
        lst.append(data.get(search_object, 0))

    for element in data.values():
        if isinstance(element, dict):
            search(element, search_object, depth - 1, lst)
    return lst


user_search = input('Введите искомый элемент: ')
search_depth = int(input('Введите глубину поиска: '))

if search(site, user_search, search_depth, lst=list()):
    print('Элемент - {} в докементе: '.format(user_search))
    for obj in search(site, user_search, search_depth, lst=list()):
        print('\t', obj)
else:
    print('В документе нет искомого элемента или малое значение глубины поиска.')

# TODO можно так:
def find_key(struct, key, max_depth=None, depth=1):
    result = None

    if max_depth and max_depth < depth:
        return result

    if key in struct:
        return struct[key]

    for sub_struct in struct.values():

        if isinstance(sub_struct, dict):

            result = find_key(sub_struct, key, max_depth, depth=depth + 1)

            if result:
                break

    return result
