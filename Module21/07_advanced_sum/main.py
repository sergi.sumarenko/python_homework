def my_sum(*args):
    summ = 0
    for obj in args:
        if type(obj) in (int, float):
            summ += obj
        elif type(obj) in (list, set, tuple):
            for element in obj:
                summ += my_sum(element)
    return summ


print(my_sum(1, 5.954, 'asd', [654, 9], (445, 844), {4555, 564}, {'sad': 4656}))
