nice_list = [1, 2, [3, 4], [[5, 6, 7], [8, 9, 10, 'hello world']],
             [[11, 12, 13, {551, 54, (564, 4)}], [14.546, 15, (15, 54)], [16, 17, 18]]]


def all_sym(data, lst=None):
    for obj in data:
        if type(obj) in (int, float, str):
            lst.append(obj)
        elif type(obj) in (list, set, tuple):
            all_sym(obj, lst)
    return lst


print(all_sym(nice_list, lst=list()))
