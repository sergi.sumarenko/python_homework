def tower_of_hanoi(numbers, first, intermediate, last):
    if numbers == 1:
        print('Переложить диск 1 со стержня номер {} на стержень номер {}'.format(first, last))
        return
    tower_of_hanoi(numbers - 1, first, last, intermediate)
    print('Переложить диск {} со стержня номер {} на стержень номер {}'.format(numbers, first, last))
    tower_of_hanoi(numbers - 1, intermediate, first, last)


quantity = int(input('Введите количество дисков: '))
tower_of_hanoi(quantity, 1, 2, 3)
