import json


def site_create(count):
    if count == 0:
        return False
    phone_name = input('Введите название телефона: ')
    site = {
        'html': {
            'head': {
                'title': 'Куплю/продам {} недорого'.format(phone_name)
            },
            'body': {
                'h2': 'У нас самая низкая цена на {}'.format(phone_name),
                'div': 'Купить',
                'p': 'продать'
            }
        }
    }
    print('Сайт для {}'.format(phone_name))
    print('site = {}'.format(
        json.dumps(site, indent=7, ensure_ascii=False)
    ))
    site_create(count - 1)


how_mach = int(input('Сколько сайтов? '))
site_create(how_mach)
