from typing import Union


class Analyses:
    """
    Класс конвертации строки в дату и анализа валидности полученых данных

     attr:
        __cls_error: (list): список из информации об ошибке полученых данных (str), используеться в __is_date_valid
        __day, __month, __year: (int) - получаемые при конвертации строки пользователя во время инициализации
    """

    __cls_error = {
        'long_month': 'Ошибка. Значение поля день за рамками допустимого диапазона 1 - 31',
        'short_month': 'Ошибка. Значение поля день за рамками допустимого диапазона 1 - 30',
        'tall_year': 'Ошибка. Значение поля день за рамками допустимого диапазона'
    }

    def __init__(self, date_string: str) -> None:
        self.__day, self.__month, self.__year = [int(num) for num in date_string.split('-')]

    @classmethod
    def __tall_year(cls, year) -> tuple[int, str]:
        """
        Проверка высокосный год или нет. Метод необходим для дальнейшего анализа кол-ва дней в месяце.
        :param year: (int) год полученый при инициализации
        :return: (tuple) состоящий из числа дней в месяце (int) а также информационной части для ошибки,
            в случае её возникновения (str)
        """
        if (year % 4 == 0) or ((year % 100 == 0) and (year % 400 == 0)):
            return 29, 'год високосный 1 - 29'
        else:
            return 28, 'год невисокосный 1 - 28'

    def _is_date_valid(self) -> Union[bool, str]:
        """
        Метод проверки валидности указаной даты.
        :param day: (int) день полученый при инициализации
        :param month: (int) месяц полученый при инициализации
        :param year: (int) год полученый при инициализации
        :return: в слечае возникновения какой либо ошибки возвращает информацию об ошбике (str),
            если ошибок в указаной дате нет возвращает True (bool) - дата валидна
        """
        error_check = True
        if 1 <= self.__month <= 12:
            if (self.__month in (1, 3, 5, 7, 8, 10, 12)) and not (1 <= self.__day <= 31):
                error_check = self.__cls_error.get('long_month')
            elif (self.__month in (4, 6, 9, 11)) and not (1 <= self.__day <= 30):
                error_check = self.__cls_error.get('short_month')
            elif (self.__month == 2) and not (1 <= self.__day <= self.__tall_year(self.__year)[0]):
                error_check = '{error}, {error_addition}'.format(
                    error=self.__cls_error.get('tall_year'),
                    error_addition=self.__tall_year(self.__year)[1]
                )
            return error_check
        else:
            return 'Ошибка. Значение поля месяц за рамками допустимого диапазона от 1 до 12'

    def __str__(self) -> str:
        return 'День: {}\tМесяц: {}\tГод: {}'.format(self.__day, self.__month, self.__year)


class Date(Analyses):
    """
    Класс инициализации родительского класса валидатора (Analyses)
    """
    @classmethod
    def from_string(cls, date_string) -> object:
        """
        Метод инициализации класса
        :param date_string: (str): дата в формате dd_mm_yyyy
        :return: (object) экземпляр класса Data с начинкой от класса 'Analyses'
        """
        return Analyses(date_string)

    @classmethod
    def is_date_valid(cls, date_string):
        """
        Метод инициализации родительского класса 'Analyses' для проверки валидности даты методом _is_date_valid
        :param date_string: (str): дата в формате dd_mm_yyyy
        :return: bool если дата валидна или str с информацией об ошибке
        """
        return Analyses(date_string)._is_date_valid()
