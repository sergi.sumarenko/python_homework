from typing import Union
from math import pi


class MyMath:

    @classmethod
    def circle_len(cls, radius: Union[int, float]) -> float:
        """
        Метод вычисления длины окружности по радиусу
        :param radius: радиус окружности
        :return: c: (float) длина окружности
        """
        c = 2 * pi * radius
        return c

    @classmethod
    def circle_sq(cls, radius: Union[int, float]) -> float:
        """
        Метод вычисления площади окружности по радиусу
        :param radius: радиус окружности
        :return: s: (float) площадь окружности
        """
        s = pi * radius ** 2
        return s

    @classmethod
    def cube_capacity(cls, length: Union[int, float]) -> Union[int, float]:
        """
        Метод вычисления обьёма куба по длине ребра
        :param length: длина ребра
        :return: s: (float) площадь окружности
        """
        v = length ** 3
        return v

    @classmethod
    def sphere_sq(cls, radius: Union[int, float]) -> float:
        """
        Метод вычисления площади поверхности сферы по радиусу
        :param radius: радиус сферы
        :return: s: (float) площадь сферы
        """
        s = pi * 4 * radius ** 2
        return s

