from typing import TextIO


class File:
    """
    Класс открытия файла

    args:
        file_name - название открываемого (создаваемого) файла
        work_mode - режим работы с файлом
    """

    def __init__(self, file_name: str, work_mode: str) -> None:
        self.__file_name = file_name
        self.__mode = work_mode

    def __enter__(self) -> TextIO:
        """
        Метод открытия файла
        :return: Если файл существует возвращает открытый файл с указаным для него методом. Если нет,
        создаёт новый файл с указаным и именем и возможностью записи в файл
        """
        try:
            return open(self.__file_name, self.__mode)
        except FileNotFoundError:
            return open(self.__file_name, 'w')

    def __exit__(self, exc_type, exc_val, exc_tb) -> bool:
        """
        Обработка любых ошибок для коректного завершения работы файлового менеджера
        :param exc_type: тип ошибки
        :param exc_val: информация об ошибке
        :param exc_tb: указатель на обьект в котором содержится информция о локализации ошибки
        :return: (bool) True
        """
        return True
