from math import sqrt
from typing import Union

class Figure:
    """
    Класс с общими методами для фигур
    :arg (str) name - название фигуры (квадрат, треугольник...)
    """
    def __init__(self, name: str) -> None:
        self.__name = name

    @property
    def name(self) -> str:
        """
        Геттер для имени класса
        :return: (str) __name - название фигуры
        """
        return self.__name


class Square(Figure):
    """
    Класс создания обьекта - квадрат
    :arg: side_length: Union[int, float] длина стороны квадрата
    """

    def __init__(self, side_length: Union[int, float]) -> None:
        super().__init__('Квадрат')
        self._side_length = side_length

    def area(self) -> Union[int, float]:
        """
        Метод вычисление площади квадрата
        :return: s: Union[int, float] площадь квадрата
        """
        s = self._side_length ** 2
        return s

    def perimeter(self) -> Union[int, float]:
        """
        Метод вычисление периметра квадрата
        :return: p: Union[int, float] периметр квадрата
        """
        p = self._side_length * 4
        return p

    @property
    def length(self) -> Union[int, float]:
        """
        Геттер длины сторон квадрата
        :return: _side_length: Union[int, float] длина стороны квадрата
        """
        return self._side_length

    @length.setter
    def length(self, new_length: Union[int, float]) -> None:
        """
        Сеттер для длины стороны квадрата
        :param new_length: Union[int, float] новая длина стороны квадрата
        """
        self._side_length = new_length


class Triangle(Figure):
    """
    Класс создания обьекта - треугольник
    :args:
        base: Union[int, float] длина основания треугольника
        height: Union[int, float] высота треугольника
    """

    def __init__(self, base, height: Union[int, float]) -> None:
        super().__init__('Треугольник')
        self._base = base
        self._height = height

    def area(self) -> Union[int, float]:
        """
        Метод вычисление площади треугольника
        :return: s: Union[int, float] площадь треугольника
        """
        s = (self._base * self._height) / 2
        return s

    def perimeter(self) -> Union[int, float]:
        """
        Метод вычисление периметра треугольника
        :return: p: Union[int, float] периметр треугольника
        """
        p = self._base * 2 * sqrt(((self._base / 2) ** 2 + self._height ** 2))
        return p

    @property
    def base(self) -> Union[int, float]:
        """
        Геттер длина основания треугольника
        :return: _base: Union[int, float] длина основания треугольника
        """
        return self._base

    @property
    def height(self) -> Union[int, float]:
        """
        Геттер высоты треугольника
        :return: _height: Union[int, float] высота треугольника
        """
        return self._height

    @base.setter
    def base(self, new_base) -> None:
        """
        Сеттер для длины основания треугольника
        :param new_base: Union[int, float] новая длина основания треугольника
        """
        self._base = new_base

    @height.setter
    def height(self, new_height) -> None:
        """
        Сеттер для высоты треугольника
        :param new_height: Union[int, float] новая высота треугольника
        """
        self._height = new_height


class Figure3D(Figure):
    """
    Класс создания 3D-обьекта

    attributes:
        __figure_list: (list): список для хранения сторон 3D-обьекта
        __area:
    :arg:
         figure: (object): 2D-фигура из которого стоится 3D-фигура
    """
    __figure_list = list()
    __area = 0

    def __init__(self, figure: object) -> None:
        if isinstance(figure, Square):
            super().__init__('Куб')
            self.__figure_list = [figure for _ in range(6)]
        elif isinstance(figure, Triangle):
            super().__init__('Пирамида')
            self.__figure_list = [figure for _ in range(4)]
            self.__figure_list.append(Square(figure.base))

    def self_area(self) -> Union[int, float]:
        """
        Метод вычисления площади 3D-обьекта
        :return: __area: Union[int, float] - площадь 3D-фигуры
        """
        for bit in self.__figure_list:
            self.__area += bit.area()
        return self.__area
