import figure


triangle = figure.Triangle(2, 5)
print('{name}:\n\tПлощадь: {area: .2f}\n\tПериметр: {perimeter: .2f}'.format(
    name=triangle.name, area=triangle.area(), perimeter=triangle.perimeter()
))

figure_3d = figure.Figure3D(triangle)
print('{name}:\n\tПлощадь: {area: .2f}'.format(
    name=figure_3d.name, area=figure_3d.self_area()
))

