def code(sym):
    if alphabet.index(sym) + step <= len(alphabet) - 1:
        sym = alphabet[alphabet.index(sym) + 3]
    else:
        sym = alphabet[alphabet.index(sym) - len(alphabet) + step]
    return sym


alphabet = [
    'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п',
    'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'
]

text = input('Введите текст: ').lower()
step = int(input('Введите шаг шифра: '))

while step > len(alphabet) or step < 0:
    print('Шаг шифра не может превышать', len(alphabet), 'или быть меньше нуля.')
    step = int(input('Введите шаг шифра: '))

code_list = [
    (code(symbol) if symbol in alphabet
        else symbol)
    for symbol in text
]

for value in code_list:
    print(value, end='')
