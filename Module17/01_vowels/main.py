vowel_list = ['а', 'у', 'о', 'ы', 'и', 'э', 'я', 'ю', 'ё', 'е']
text = input('Введите текст: ').lower()

vowel_list = [sym for sym in text if sym in vowel_list]

print('Список гласных букв:', vowel_list)
print('Длина списка:', len(vowel_list))

