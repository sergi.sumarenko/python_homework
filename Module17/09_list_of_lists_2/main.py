nice_list = [[[1, 2, 3], [4, 5, 6], [7, 8, 9]],
             [[10, 11, 12], [13, 14, 15], [16, 17, 18]]]

new_list = [value for first_lvl in nice_list
            for second_lvl in first_lvl
            for value in second_lvl]

print(new_list)
