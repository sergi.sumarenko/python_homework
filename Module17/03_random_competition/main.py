import random

first_list = [round(random.uniform(5, 10), 2) for _ in range(10)]
second_list = [round(random.uniform(5, 10), 2) for _ in range(10)]
winners_list = [max(first_list[i_member], second_list[i_member]) for i_member in range(10)]

print('Первая команда', first_list)
print('\nВторая команда', second_list)
print('\nСписок победителей:', winners_list)
