import random

initial_list = [random.randint(0, 5) for _ in range(25)]

print('Изначальный список:', initial_list)

modific_list = initial_list[:]

for num in modific_list:
    if num == 0:
        modific_list += [modific_list.pop(modific_list.index(num))]

print('Модифицированый список (все 0 перенесены в конец):', modific_list)

del modific_list[-modific_list.count(0):]

print('Модифицированый список (все 0 удалены):', modific_list)
