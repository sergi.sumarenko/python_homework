import random


def pnt(lst):
    for sym in lst:
        print(sym, end='')
    print()


amount_sticks = int(input('введите количество палок: '))
amount_throws = int(input('Введите кол-во бросков: '))

sticks = ['!' for _ in range(amount_sticks)]

for i_throws in range(amount_throws):
    start = random.randint(1, amount_sticks)
    stop = random.randint(start, amount_sticks)
    print(f'Были сбиты паки с {start} по {stop}')
    sticks[start - 1:stop] = '_' * (stop + 1 - start)
    pnt(sticks)
