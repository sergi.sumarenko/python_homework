def f_summ(x):
    summ = 0
    while x != 0:
        summ += x % 10
        x //= 10
    return summ


def f_count(y):
    count = 0
    while y != 0:
        count += 1
        y //= 10
    return count


first_num = int(input('Введите целове положительное число: '))
while first_num <= 0:
    print('Введено некоректоное число!')
    first_num = int(input('Введите целове положительное число: '))

print('Сумма цифр:', f_summ(first_num))
print('Количество цифр в числе:', f_count(first_num))
print('Разность суммы и кол-ва цифр:', f_summ(first_num) - f_count(first_num))
