def conversely(x):
    revers_num = ''
    for symbol in str(x).split('.'):
        revers_num += symbol[::-1] + '.'
    return float(revers_num[:-1])


N = float(input('Введите первое число: '))
K = float(input('Введите второе число: '))
print('Первое число наоборот', conversely(N))
print('Второе число наоборот', conversely(K))
print('Сумма:', conversely(N) + conversely(K))
