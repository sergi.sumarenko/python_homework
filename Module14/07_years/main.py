start = int(input('Введите начало периода: '))
stop = int(input('Введите конец периода: '))
count = 0
print(f'Года от {start} до {stop} с тремя одинаковами цифрами:')

while start != stop:
    for num in range(0, 10):
        for symbol in str(start):
            if symbol == str(num):
                count += 1
        if count == 3:
            print(start)
        count = 0
    start += 1
