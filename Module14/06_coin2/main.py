import math

X = float(input('Введите координаты оси X: '))
Y = float(input('Введите координаты оси Y: '))
r = float(input('Введите радиус металлоискателя: '))

if r >= math.sqrt(X ** 2 + Y ** 2):
    print('Монетка где-то рядом')
else:
    print('Монетки рядом нет')
