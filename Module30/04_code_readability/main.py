one_str = list(filter(lambda n: n in (2, 3, 5, 7) or
                      not (n == n and tuple(filter(lambda y: n % y == 0, (2, 3, 5, 7)))),
                      (n for n in range(1001))))
print(one_str)


def check(check_num):
    denominator_list = (2, 3, 5, 7)
    for denominator in denominator_list:
        if check_num not in denominator_list and check_num % denominator == 0:
            return False
    return True


for num in range(2, 1001):
    if check(num):
        print(num, end=', ')
