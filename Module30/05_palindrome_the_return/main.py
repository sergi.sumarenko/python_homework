from collections import Counter


def can_be_poly(user_input):
    return len(list(filter(lambda num: num % 2 != 0, Counter(user_input).values()))) <= 1


print(can_be_poly('ababc'))
print(can_be_poly('abbbc'))