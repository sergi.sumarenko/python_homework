from timeit import timeit

result = timeit('"-".join(str(n) for n in range(100))', number=10000)
print(result)

list_comp = timeit('s=""\n[(s := s + str(i)) for i in range(100)][-1]', number=10000)
print(list_comp)

reduce_method = timeit('from functools import reduce\nreduce(lambda f, j: str(f) + str(j), (n for n in range(100)))',
                       number=10000)
print(reduce_method)

map_method = timeit('"-".join(map(lambda x: str(x), (x for x in range(100))))', number=10000)
print(map_method)