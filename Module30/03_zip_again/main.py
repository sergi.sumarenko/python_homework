from typing import List


strings: List[str] = ['a', 'b', 'c', 'd', 'e']
numbers: List[str] = [1, 2, 3, 4, 5, 6, 7, 8]

result: List[str] = list(map(lambda s, n: (s, n), strings, numbers))
print(result)
