cells = int(input('Количество клеток: '))
bad_cells = []

for i in range(cells):
    cell_efficiency = int(input(f'Эффективность {i + 1} клетки: '))
    if cell_efficiency < i:
        bad_cells.append(cell_efficiency)

print('Неподходящие значение: ', end=' ')
for sym in bad_cells:
    print(sym, end=' ')
