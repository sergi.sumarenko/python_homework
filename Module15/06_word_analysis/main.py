text = input('Введите анализируемый текст: ')
symbols = list(text)
count = 0
check = True

for i in range(len(symbols)):
    for sym in range(len(symbols)):
        if (symbols[i] == symbols[sym]) and (i != sym):
            check = False
            break
    if check:
        count += 1
    check = True

print('Количество уникальных букв:', count)
