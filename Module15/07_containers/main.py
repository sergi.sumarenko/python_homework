containers_list = []

containers = int(input('Введите кол-во контейнеров: '))

for _ in range(containers):
    weigh = int(input('Введите вес контейнера: '))
    while weigh > 200 or weigh <= 0:
        print('Ошибка ввода, вес не может превышать 200кг или быть меньше или равен 0')
        weigh = int(input('Введите вес контейнера: '))
    containers_list.append(weigh)

new_container = int(input('Введите вес нового контейнера: '))

if new_container in containers_list:
    containers_list.insert(containers_list.count(new_container), new_container)
else:
    containers_list.append(new_container)

containers_list.sort(reverse=True)

indx = max([index for index, n in enumerate(containers_list) if n == new_container]) + 1

print('Номер, куда вставлен новый контейнер', indx)
