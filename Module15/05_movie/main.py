films = ['Крепкий орешек', 'Назад в будущее', 'Таксист',
         'Леон', 'Богемская рапсодия', 'Город грехов',
         'Мементо', 'Отступники', 'Деревня']
favorite_films = []

film = input('Введите название фильма или 0 для завершения: ')

while film != '0':
    if film.capitalize() in films:
        favorite_films.append(film)
        print('Фильм добавлен в список любимых.')
    elif film.capitalize() not in films:
        print('Данный фильм отсутствует.')
    film = input('Введите название фильма или 0 для завершения: ')

print('Список любимых фильмов:')

for sym in favorite_films:
    print(sym)
