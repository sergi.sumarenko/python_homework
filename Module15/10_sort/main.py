def my_sort(x):
    for i in range(len(x)):
        count = x.index(min(x[i:len(x)]))
        x.insert(i, x[count])
        del x[count + 1]
    return x


num_list = [1, 4, -3, 0, 10]

print('\nИзначальный список:', num_list)
print('Отсортированый список:', my_sort(num_list))      # num_list.sort()
