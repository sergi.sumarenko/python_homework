cards = int(input('Введите кол-во видеокарт: '))
cards_list = []

for i in range(cards):
    num_card = int(input(f'{i + 1} Видеокарта: '))
    cards_list.append(num_card)

print('Старый список видеокарт: ', cards_list)

m = max(cards_list)

for sym in cards_list:
    if sym == m:
        cards_list.remove(sym)

print('Новый список видеокарт:', cards_list)
