num_list = []
step = int(input('Сдвиг: '))
total_num = int(input('Введите количество чисел в списке: '))

for _ in range(total_num):
    num = int(input('Введите число из списка: '))
    num_list.append(num)

print('Изначальный список:', num_list)

for _ in range(step):
    num_list.insert(0, num_list[len(num_list) - 1])
    num_list.pop(len(num_list) - 1)

print('Сдвинутый список:', num_list)
