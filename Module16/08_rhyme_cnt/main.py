people = []
count = 0
total_people = int(input('Количество человек: '))
chs = int(input('Какое число в считалке? '))

people.extend(range(1, total_people + 1))

for _ in range(total_people - 1):
    print('\nТекущий круг людей:', people)
    print('Начало счёта с номера', people[count])
    delete = (count + chs - 1) % len(people)
    if people[delete] == people[-1]:
        count = 0
    else:
        count = ((count + chs - 1) % len(people))
    print('Выбывает человек под номером', people.pop(delete))

print('Остался человек под номером:', people)
