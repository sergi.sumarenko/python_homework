def time_count(t, ch, title):
    for i_box in range(len(violator_songs)):
        if title.lower() == violator_songs[i_box][0].lower():
            t += violator_songs[i_box][1]
            ch = True
    return t, ch


violator_songs = [
    ['World in My Eyes', 4.86],
    ['Sweetest Perfection', 4.43],
    ['Personal Jesus', 4.56],
    ['Halo', 4.9],
    ['Waiting for the Night', 6.07],
    ['Enjoy the Silence', 4.20],
    ['Policy of Truth', 4.76],
    ['Blue Dress', 4.29],
    ['Clean', 5.83]
]

total_time = 0
check = False

song_ctr = int(input('Сколько песен выбрать? '))

for i_song in range(song_ctr):
    title_song = input(f'Название {i_song + 1} песни: ')
    total_time, check = time_count(total_time, check, title_song)
    while not check:
        print(f'Песня {title_song} не найдена.')
        title_song = input(f'Название {i_song + 1} песни: ')
        total_time, check = time_count(total_time, check, title_song)
    check = False

print(f'\nОбщее время звучания песен: {round(total_time, 2)} минут.')
