skates = []
people = []
count = 0

amount_skates = int(input('Кол-во коньков: '))

for i_skates in range(amount_skates):
    skates_size = int(input(f'Размер {i_skates + 1} пары: '))
    skates.append(skates_size)

amount_people = int(input('\nКол-во людей: '))

for i_people in range(amount_people):
    people_size = int(input(f'Размер ноги {i_people + 1} человека: '))
    people.append(people_size)

skates.sort()
people.sort()

for i_pl in range(len(people)):
    for i_sk in range(len(skates)):
        if people[i_pl] <= skates[i_sk]:
            count += 1
            skates.pop(i_sk)
            break

print('\nНаибольшее кол-во людей, которые могут взять ролики:', count)
