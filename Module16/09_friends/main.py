friends_list = []

friends = int(input('Сколько всего друзей: '))

for i_frd in range(friends):
    friends_list.append([i_frd + 1, 0])

listing = int(input('Сколько всего рассписок: '))

for _ in range(listing):
    whom = int(input('\nКому: '))
    who = int(input('От кого: '))
    how = int(input('Сколько: '))
    friends_list[whom - 1][1] -= how
    friends_list[who - 1][1] += how

print()
for i in range(friends):
    print(f'{friends_list[i][0]} : {friends_list[i][1]}')
