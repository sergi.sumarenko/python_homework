def num_for_list(num_lst, count, lst):
    print(num_lst, 'список')
    for i_num in range(count):
        num = int(input(f'Введите {i_num + 1} число: '))
        lst.append(num)
    print(lst)


first_list = []
second_list = []
sym_count = 0

num_for_list('Первый', 3, first_list)
num_for_list('Второй', 7, second_list)

first_list.extend(second_list)

for sym in first_list:
    sym_count += first_list.count(sym)
    for _ in range(sym_count - 1):
        first_list.remove(sym)
    sym_count = 0

print(first_list)
