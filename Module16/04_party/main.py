guests = ['Петя', 'Ваня', 'Саша', 'Лиза', 'Катя']

print(f'Сейчас на вечеринке {len(guests)} человек:', guests)

question = input('Гость пришел или ушел? ').lower()

while question != 'пора спать':
    name = input('Имя гостя: ').capitalize()
    if question == 'пришел' and len(guests) < 6:
        print(f'Привет, {name}!')
        guests.append(name)
    elif question == 'ушел':
        if name in guests:
            print(f'Пока, {name}!')
            guests.remove(name)
        else:
            print('Такого здесь не было.')
    elif len(guests) == 6:
        print(f'Прости, {name}, но мест нет.')
    print(f'\nСейчас на вечеринке {len(guests)} человек:', guests)
    question = input('Гость пришел или ушел? ').lower()

print('Вечеринка закончилась, все легли спать.')
