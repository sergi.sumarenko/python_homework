num_list = []
count = 0

amount_num = int(input('Кол-во чисел: '))

for _ in range(amount_num):
    num = int(input('Число: '))
    num_list.append(num)

for i_num in range(len(num_list), 0, -1):
    if num_list[i_num - 1] == num_list[len(num_list) - 1]:
        count += 1
    else:
        break

print('Нужно прописать чисел:', len(num_list) - count)
print('Сами числа: ', end='')

for i_num in range(len(num_list) - 1 - count, -1, -1):
    print(num_list[i_num], end=' ')
    num_list.append(num_list[i_num])

print('\nСимметричная последовательность:', num_list)
