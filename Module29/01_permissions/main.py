from typing import Callable, Union
import functools


permission_info = {
    'delete_site': {'admin'},
    'add_comment': {'admin', 'user_1'}
}


def check_permission(user: str) -> Callable:

    def decorate(func: Callable) -> Callable:
        @functools.wraps(func)
        def wrapper(*args, **kwargs) -> Union[Callable, Exception]:
            if user in permission_info.get(func.__name__):
                return func(*args, **kwargs)
            else:
                raise PermissionError('У пользователя недостаточно прав, чтобы выполнить функцию {func_name}'.format(
                    func_name=func.__name__
                ))
        return wrapper
    return decorate


@check_permission('admin')
def delete_site():
    print('Удаляем сайт')


@check_permission('admin')
def add_comment():
    print('Добавляем коментрий')


delete_site()
add_comment()
