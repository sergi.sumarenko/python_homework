from datetime import datetime
from time import time
from typing import Callable


def log_decor(date_format: str) -> Callable:
    def logging(method: Callable) -> Callable:

        def wrapper(*args, **kwargs) -> None:
            meth_name = str(method).split()[1]
            date = ''.join('%' + sym if sym.isalpha() else sym for sym in date_format)
            print('- Запускается \'{name}\'. дата и время запуска: {date}'.format(
                name=meth_name, date=datetime.now().strftime(date),
            ))
            start = time()
            print('Результат работы {name}: {result}'.format(
                name=meth_name, result=method(*args, **kwargs)
            ))
            print('- Завершение \'{name}\', время работы = {time: .3f}s'.format(
                name=meth_name, time=time() - start))

        return wrapper
    return logging


def log_methods(date_f: str, *, log: Callable = log_decor):

    def obj(cls: Callable) -> Callable:

        for cur_m in dir(cls):
            if not cur_m.startswith('__'):
                cur_method = getattr(cls, cur_m)
                decor_method = log(date_f)(cur_method)
                setattr(cls, cur_m, decor_method)

        return cls
    return obj


@log_methods("b d Y - H:M:S")
class A:

    def test_sum_1(self) -> int:
        print('test sum 1')
        number = 100
        result = 0
        for _ in range(number + 1):
            result += sum([i_num ** 2 for i_num in range(10000)])

        return result


@log_methods("b d Y - H:M:S")
class B(A):

    def test_sum_1(self):
        super().test_sum_1()
        print("Наследник test sum 1")

    def test_sum_2(self):
        print("test sum 2")
        number = 200
        result = 0
        for _ in range(number + 1):
            result += sum([i_num ** 2 for i_num in range(10000)])

        return result


my_obj = B()
my_obj.test_sum_1()
my_obj.test_sum_2()
