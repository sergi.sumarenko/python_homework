from typing import Callable


def decorator_with_args_for_any_decorator(func):

    def first_wrapper(*args, **kwargs):
        print('Переданные арги и кварги в декоратор:', args, kwargs)
        return func
    return first_wrapper


@decorator_with_args_for_any_decorator
def decorated_decorator(func: Callable):

    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper


@decorated_decorator(100, 'рублей', 200, 'друзей')
def decorated_function(text: str, num: int) -> None:
    print("Привет", text, num)


decorated_function("Юзер", 101)
