from typing import Callable

app = dict()


def callback(path: str) -> Callable:

    def current_func(func: Callable) -> Callable:

        def wrapper() -> None:

            app[path] = func

        return wrapper
    return current_func


@callback('//')
def example() -> str:
    print('Пример функции, котороя возвращает ответ сервера')
    return 'ОК'


example()

route = app.get('//')
if route:
    response = route()
    print('Ответ:', response)
else:
    print('Такого пути нет')

