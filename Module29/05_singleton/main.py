from typing import Callable


def singleton(cls: Callable) -> Callable:
    single = dict()

    def cls_wrapper(*args, **kwargs) -> Callable:
        if cls not in single:
            single[cls] = cls(*args, **kwargs)
        return single.get(cls)
    return cls_wrapper


@singleton
class Example:

    def __init__(self, name: str) -> str:
        self.__name = name

    @property
    def name(self) -> str:
        return self.__name


my_obj = Example('first')
my_another_obj = Example('second')

print(my_obj.name)
print(my_another_obj.name)

print(id(my_obj))
print(id(my_another_obj))


print(my_obj is my_another_obj)
