from tkinter import *
import random


class Main:
    name = None
    sym = None
    winning_combo = ({1, 4, 7}, {1, 5, 9}, {1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {2, 5, 8}, {3, 6, 9}, {3, 5, 7})
    user_choice = set()
    btn_lst = list()

    def __init__(self):
        self.auth_window = Tk()

        self.auth_window.title('Залогинся')
        self.auth_window.geometry('400x150')

        Label(self.auth_window, text='Имя: ').grid(column=0, row=0)
        self.in_name = Entry(self.auth_window, width=20, bg='white', fg='black', borderwidth=5)
        self.in_name.grid(column=1, row=0)

        Label(self.auth_window, text='Крестик или нолик? ').grid(column=0, row=1)
        self.in_sym = Entry(self.auth_window, width=20, bg='white', fg='black', borderwidth=5)
        self.in_sym.grid(column=1, row=1)

        self.user_btn = Button(self.auth_window, text='Вход', bg='green', command=self.start_play)
        self.user_btn.grid()

        self.auth_window.mainloop()

    def start_play(self):
        Main.name = self.in_name.get()
        user_sym = self.in_sym.get()

        self.auth_window.destroy()

        game_window = Tk()
        game_window.title('Крестики/нолики')
        game_window.geometry('615x525')

        for i in range(1, 4):
            for j in range(1, 4):
                Btn(game_window, user_sym, self.user_choice, Comp.comp_choice, i, j)


class Btn:
    num = 1
    obj_lst = {
        'btns': dict(),
        'label': list()
    }

    def __init__(self, window, user_sym, user_choice, comp_choice, n_col, n_row):
        self.window = window
        self.user_sym = user_sym
        self.user_choice = user_choice
        self.comp_choice = comp_choice
        self.n_col = n_col
        self.n_row = n_row
        self.btn = Button(
            self.window,
            bg='black', height=9, width=20, borderwidth=5,
            command=self.choice
        )
        self.btn.grid(column=self.n_col, row=self.n_row)
        self.num = Btn.num
        self.obj_lst['btns'][self.num] = (self.btn, self.n_col, self.n_row)
        Btn.num += 1

    def choice(self):
        self.user_choice.add(self.num)
        self.obj_lst['btns'].pop(self.num)
        self.btn.destroy()
        if self.user_sym == 'крестик':
            lbl = Label(self.window, text='X', borderwidth=5, font=("Arial Bold", 65), fg='green', width=3, height=1)

        else:
            lbl = Label(self.window, text='O', borderwidth=5, font=("Arial Bold", 65), fg='green', width=3, height=1)
        lbl.grid(column=self.n_col, row=self.n_row)
        self.obj_lst['label'].append(lbl)
        self.winner_check()

    def winner_check(self):
        for combo in Main.winning_combo:
            if len(combo & self.user_choice) == 3:
                ResultWindow(self.obj_lst, self.window).winner('победил', 'green')
                break
        else:
            if len(self.obj_lst.get('btns').keys()) != 0:
                Comp(self.obj_lst, self.user_sym, self.window, self.user_choice)
            else:
                ResultWindow(self.obj_lst, self.window).draw()
            for combo in Main.winning_combo:
                if len(combo & self.comp_choice) == 3:
                    ResultWindow(self.obj_lst, self.window).winner('проиграл', 'red')
                    break


class ResultWindow:

    def __init__(self, obj_lst, window):
        self.window = window
        self.obj_lst = obj_lst
        self.destr()

    def destr(self):
        for val in self.obj_lst['btns'].values():
            val[0].destroy()
        for val in self.obj_lst['label']:
            val.destroy()

    def winner(self, res, color):
        Label(
            self.window,
            text="{},\nты {}!".format(Main.name.capitalize(), res),
            font=("Arial Bold", 45), fg=color
        ).grid(row=0)

    def draw(self):
        Label(
            self.window,
            text="{}, ничья!".format(Main.name.capitalize()),
            font=("Arial Bold", 45), fg='yellow'
        ).grid(row=0)


class Comp:
    comp_choice = set()

    def __init__(self, obj_lst, user_sym, window, user_choice):
        self.obj_lst = obj_lst
        self.user_sym = user_sym
        self.window = window
        self.user_choice = user_choice

        comp_step = self.comp_analysis()

        self.comp_choice.add(comp_step)

        self.obj_lst.get('btns')[comp_step][0].destroy()
        if self.user_sym == 'крестик':
            lbl = Label(
                self.window,
                text='O', font=("Arial Bold", 65), fg='red',
            )

        else:
            lbl = Label(
                self.window,
                text='X', borderwidth=5, font=("Arial Bold", 65),
                fg='red', width=3, height=1
            )
        lbl.grid(column=self.obj_lst.get('btns')[comp_step][1], row=self.obj_lst.get('btns')[comp_step][2])
        self.obj_lst.get('btns').pop(comp_step)
        self.obj_lst['label'].append(lbl)

    def comp_analysis(self):
        try:
            num_lst = set()

            for combo in Main.winning_combo:
                if len(combo & self.user_choice) > 1:
                    element = combo.copy()
                    element &= set(self.obj_lst['btns'].keys())
                    for num in element:
                        num_lst.add(num)

            return random.choice(list(num_lst - self.comp_choice))
        except IndexError:
            return random.choice(list(self.obj_lst['btns'].keys()))
