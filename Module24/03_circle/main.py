import math


class Circle:

    def __init__(self, x=0, y=0, r=1):
        self.x = x
        self.y = y
        self.r = r

    def area(self):
        s = math.pi * self.r ** 2
        return s

    def circumference(self):
        c = 2 * math.pi * self.r
        return c

    def grow(self, k):
        self.r = self.r * k

    def intersection(self, obj):
        d = math.sqrt((obj.x - self.x) ** 2 + (obj.y - self.x) ** 2)
        if obj.r + self.r < d:
            print('Окружности не пересекаются')
        elif math.fabs(self.r - obj.r) > d:
            print('Одно из окружностей находится внутри другой')
        else:
            print('окружности пересекаются')


first = Circle(5, 5, 2)
second = Circle(1, 1, 3)
first.intersection(second)