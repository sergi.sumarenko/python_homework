class Student:

    def __init__(self, name_surname=None, group_num=None, balls=None):
        self.name_surname = name_surname
        self.group_num = group_num
        ball_lst = [int(num) for num in balls]
        self.progress = sum(ball_lst) / 5

    def info(self):
        print('|{: ^25}|{: ^10}|{: ^16}|'.format(
            self.name_surname, self.group_num, self.progress
        ))


with open('students.txt', 'r') as student_list:
    student_lst = sorted([
        Student(' '.join(string.split()[0:2]), string.split()[2], string.split()[3:])
        for string in student_list.readlines()
    ], key=lambda item: item.progress, reverse=True)

print('|{: ^25}|{: ^10}|{: ^16}|'.format(
    'Имя, фамилия', 'Группа', 'Успеваемость'
))

for n in student_lst:
    n.info()
