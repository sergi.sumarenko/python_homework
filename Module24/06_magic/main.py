class Water:
    name = 'Вода'

    def __add__(self, other):
        return AddElements(self, other)


class Air:
    name = 'Воздух'

    def __add__(self, other):
        return AddElements(self, other)


class Earth:
    name = 'Земля'

    def __add__(self, other):
        return AddElements(self, other)


class Fire:
    name = 'Огонь'

    def __add__(self, other):
        return AddElements(self, other)


class Aether:
    name = 'Эфир'

    def __add__(self, other):
        return AddElements(self, other)


class AddElements:
    answer = None

    def __init__(self, first, second):
        self.first = first
        self.second = second
        try:
            self.check(Water, Fire, 'Пар')
            self.check(Water, Air, 'Шторм')
            self.check(Water, Earth, 'Грязь')
            self.check(Air, Fire, 'Молния')
            self.check(Air, Earth, 'Пыль')
            self.check(Fire, Earth, 'Лава')
        except TimeoutError:
            ...

    def check(self, first_element, second_element, out):
        try:
            first = self.first.name
            second = self.second.name
            if 'Эфир' in (first, second):
                self.answer = 'Magic'
            else:
                if (first != second) and first in (first_element.name, second_element.name) and \
                        second in (first_element.name, second_element.name):
                    self.answer = out
                elif first == second:
                    self.answer = '{} в квадрате'.format(first)
                raise TimeoutError
        except AttributeError:
            ...


a = Aether()
b = Fire()

c = b + b
print(c.answer)
