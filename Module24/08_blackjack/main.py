import black

card_list = '2 3 4 5 6 7 8 9 10 Д В К Т'.split()
game_flag = True
output = False

deck_dict = {
    'Бубна': card_list,
    'Крести': card_list,
    'Чирва': card_list,
    'Пики': card_list
}

game_dct = {
    'comp': {
        'total_bal': 0,
        'cards': list()
    },

}

user_name = input('Введите имя: ').capitalize()

game_dct[user_name] = {'total_bal': 0, 'cards': list()}

first_game = black.Game(deck_dict, game_dct)

for name in game_dct.keys():
    if first_game.check_result(name) == 21:
        print('{} победил у него Blackjack')
        break
else:
    first_game.print_result(user_name)
    while game_flag:
        question = input('{} взять еще карту (1. Да // 2. Хватит)? '.format(user_name))
        if question == '1':
            first_game.choice(user_name)
            first_game.print_result(user_name)
            if first_game.check_result(user_name) > 21:
                print('Много')
                game_flag = False
        else:
            game_flag = False
        if first_game.check_result('comp') < 17:
            first_game.choice('comp')
            if first_game.check_result('comp') > 21:
                print('У дилера перебор.\n{} ты победил.'.format(user_name))
if (first_game.check_result('comp') and first_game.check_result(user_name)) <= 21:
    print('У вас набралось: {}, карты: {}.\nУ дилера набралось: {}, карты: {}'.format(
        first_game.check_result(user_name),
        first_game.print_result_min(user_name),
        first_game.check_result('comp'),
        first_game.print_result_min('comp'),
    ))
    if first_game.check_result(user_name) > first_game.check_result('comp'):
        print('{} ты победил.'.format(user_name))
    elif first_game.check_result(user_name) < first_game.check_result('comp'):
        print('{} ты проиграл.'.format(user_name))
    else:
        print('Ничья')

