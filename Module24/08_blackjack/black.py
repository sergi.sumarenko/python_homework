import random


class CardSuit:

    def __init__(self, suit, value):
        print('+' + '-' * 18 + '+')
        print('|{value}{: ^{wight}}{value}|'.format(
            ' ', value=value, wight=(18 - len(value) * 2)
        ))
        if suit == 'Бубна':
            Diamond()
        elif suit == 'Крести':
            Clubs()
        elif suit == 'Чирва':
            Hearts()
        elif suit == 'Пики':
            Pikes()
        print('|{value}{: ^{wight}}{value}|'.format(
            ' ', value=value, wight=(18 - len(value) * 2)
        ))
        print('+' + '-' * 18 + '+')


class Diamond:
    count = 0

    def __init__(self):
        while self.count <= 4:
            print('|{: ^18}|'.format('*' * (1 + self.count)))
            self.count += 2

        while self.count > -1:
            print('|{: ^18}|'.format('*' * (1 + self.count)))
            self.count -= 2


class Clubs:

    def __init__(self):
        for i in (3, 7, 5, 0, 16, 0, 3, 5):
            if i == 0:
                print('|{: ^18}|'.format('***' + '   ' + '*' + '    ' + '***'))
            else:
                print('|{: ^18}|'.format('*' * i))


class Hearts:

    def __init__(self):
        print('|{: ^18}|'.format('*' * 4 + ' ' * 2 + '*' * 4))
        for i in range(6, 0, -1):
            print('|{: ^18}|'.format('*' * i * 2))


class Pikes:

    def __init__(self):
        for i in (1, 5, 9, 13, 13, 0, 3, 5):
            if i == 0:
                print('|{: ^18}|'.format('*' * 4 + ' * ' + '*' * 4))
            else:
                print('|{: ^18}|'.format('*' * i))


class Game:
    count = 0

    def __init__(self, deck_dict, game_dct):
        self.deck_dict = deck_dict
        self.game_dct = game_dct
        self.start_play()

    def start_play(self):
        for _ in range(2):
            for name in (self.game_dct.keys()):
                self.choice(name)

    def choice(self, name):
        suit = random.choice(list(self.deck_dict.keys()))
        value = random.choice(self.deck_dict.get(suit))
        self.deck_dict.get(suit).remove(value)

        if len(self.deck_dict.get(suit)) == 0:
            self.deck_dict.pop(suit)

        self.cards_in_game(name, suit, value)

    def cards_in_game(self, name, suit, value):
        self.game_dct[name]['cards'].append((suit, value))

        if value.isdigit():
            self.game_dct[name]['total_bal'] += int(value)
        elif value == 'Т':
            if self.game_dct[name]['total_bal'] <= 21:
                self.game_dct[name]['total_bal'] += 11
            else:
                self.game_dct[name]['total_bal'] += 1
        else:
            self.game_dct[name]['total_bal'] += 10

    def print_result(self, name):
        print('Карты на руках:')
        for card in self.game_dct.get(name, None).get('cards'):
            CardSuit(card[0], card[1])

        print('\nОбщие кол-во очков для {}: {}'.format(
            name,
            self.game_dct.get(name, None).get('total_bal')
        ))

    def print_result_min(self, name):
        str = ''
        for card in self.game_dct.get(name, None).get('cards'):
            str += '{}: {}; '.format(card[0], card[1])
        return str

    def check_result(self, name):
        return self.game_dct.get(name, None).get('total_bal')
