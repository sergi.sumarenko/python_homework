import ferma


def menu(act):
    if act == '0':
        user.plant()
    elif act == '1':
        user.look_after()
    elif act == '2':
        garden_bit_two = input('С какого участка убрать жуков? ~ ')
        user.look_after_two(garden_bit_two)
    elif act == '3':
        garden_bit = input('С какого участка собрать уражай? ~ ')
        user.harvest(garden_bit)
    elif act == '4':
        raise TimeoutError
    else:
        print('Фермер не знает, что делать')


user_name = input('Введите ваше имя: ')
first_garden = ferma.Garden(
    int(input('{}, сколько соток земли вам выделить? ~'.format(user_name)))
)
user = ferma.Farmer(user_name, first_garden)

while True:
    to_do = input('0. Посадить\t1. Проверить плантацию\t2. Убрать жуков'
                  '\t3. Собрать уражай\t4. Выйти\n~')
    try:
        menu(to_do)
    except TimeoutError:
        print('{}, твой урожай составил:'.format(user_name))

        if len(user.storage) == 0:
            print('\tУражая нет.')
        else:
            for obj, value in user.storage.items():
                print('\t{}: {} кг.'.format(obj.capitalize(), value))

        break
