from time import sleep
from threading import Thread
from random import randint


class Potato(Thread):
    name = ('картошка', 'картошку', 'картошкой')
    status = ('Росток', 'Цветёт', 'Созрела')
    current_status = 'Саженец'
    look_after_flag = False

    def __init__(self, place=None):
        super(Potato, self).__init__()
        self.place = place

    def run(self):
        for status_value in self.status:
            sleep(randint(3, 5))
            if randint(1, 8) == 3:
                self.current_status = status_value + ' но, появились жуки.'
                for _ in range(5):
                    sleep(5)
                    if self.look_after_flag:
                        print('Жуки убиты!')
                        break
                else:
                    self.current_status = 'Испорчена'
                    break
            self.current_status = status_value

    def check_status(self):
        print('Картошка на участке {} сейчас {}'.format(self.place, self.current_status))


class Garden:
    all_plants = dict()

    def __init__(self, garden_size=None):
        self.garden_size = garden_size
        for i_place in range(1, garden_size + 1):
            self.all_plants[str(i_place)] = 'Пусто'

    def add_vegetable(self, num_place, vegetable):
        self.all_plants[num_place] = vegetable
        self.all_plants[num_place].start()
        print('{} посажена'.format(vegetable.name[0]))

    def remove_vegetable(self, num_place):
        self.all_plants.pop(num_place)


class Farmer:
    storage = dict()

    def __init__(self, name, own_garden):
        self.name = name
        self.own_garden = own_garden
        self.garden_dict = own_garden.all_plants

    def plant(self):
        vegetable = input('Что посадить? ')
        if vegetable not in Potato.name:
            print('Нет таких саженцов')
        else:
            work_place = input('Номер плантации (111 - что бы засадить всю плантацию {}): '.format(
                Potato.name[2]
            ))
            if work_place == 'всю':
                for i_place in self.garden_dict:
                    if self.garden_dict.get(i_place) == 'Пусто':
                        self.own_garden.add_vegetable(i_place, Potato(i_place))
            else:
                if 0 < int(work_place) <= self.own_garden.garden_size:
                    self.own_garden.add_vegetable(work_place, Potato(work_place))
                else:
                    print('Вне зоны нашей плантации')

    def look_after(self):
        for key, vegetable in self.garden_dict.items():
            try:
                vegetable.check_status()
            except AttributeError:
                print('На участке №{} {}'.format(key, vegetable))

    def look_after_two(self, work_place):
        if work_place in self.garden_dict.keys():
            if 'жуки' in self.garden_dict[work_place].current_status:
                self.garden_dict[work_place].look_after_flag = True

    def harvest(self, garden_bit):
        if garden_bit in self.garden_dict.keys():
            work_place = self.garden_dict.get(garden_bit)

            if work_place.current_status == 'Созрела':

                if work_place.name[0] in self.storage.keys():
                    self.storage[work_place.name[0]] += randint(5, 10)
                else:
                    self.storage[work_place.name[0]] = randint(5, 10)

                self.own_garden.remove_vegetable(garden_bit)
                print('{} собрана, на складе уже {} кг.'.format(
                    work_place.name[0],
                    self.storage[work_place.name[0]]
                ))

            elif work_place.current_status in ('Росток', 'Цветёт'):
                print('Еще не созрела')
            else:
                print('Грядку сьели жуки')
                self.own_garden.remove_vegetable(garden_bit)
        else:
            print('Грядка не найдена')
