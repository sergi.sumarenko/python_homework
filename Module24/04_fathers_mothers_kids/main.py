import family


def find_children(act_name, act, children=None):
    child_name = input('Имя ребёнка, которого нужно {}? '.format(act_name))
    for child in children:
        if child_name.capitalize() in child.name:
            act(child)


def mother_act(children):
    act = input('1. Проверить детей\t2. Успокоить\t3. Накормить\t4. Выйти\n~ ')
    if act == '1':
        mother.check_children(children)
    elif act == '2':
        find_children('Успокоить', mother.calm_child, children)
    elif act == '3':
        find_children('Накормить', mother.feed_child, children)
    elif act == '4':
        for child in children:
            child.ext = True
        raise TimeoutError
    else:
        print('Растерялась...')


mother = family.Parents('Ann', 35, 'Денис', 'Кристина')
mother.info()
den = family.Children('Денис', 12)
kristi = family.Children('Кристина', 10)

den.start()
kristi.start()

while True:
    try:
        mother_act((den, kristi))
    except TimeoutError:
        break
