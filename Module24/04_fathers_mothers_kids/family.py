import random
from threading import Thread
from time import sleep


class Parents:

    def __init__(self, name=None, age=None, *args):
        super(Parents, self).__init__()
        self.name = name
        self.age = age
        self.children = [name for name in args]

    def info(self):
        print('Имя: {}\nВозраст: {}\nДети: {}'.format(
            self.name, self.age, ', '.join(self.children)
        ))

    def calm_child(self, child_name):
        child_name.current_status = 'Спокойный(ая)'
        print('{} успокоен(а)'.format(child_name.name))

    def feed_child(self, child_name):
        child_name.current_status = 'Спокойный(ая)'
        print('{} накормлен(а)'.format(child_name.name))

    def check_children(self, lst=None):
        for child in lst:
            print('{} {}'.format(child.name, child.current_status))


class Children(Thread):
    status = ('Голодный(ая)', 'Разбушевался(ась)', 'Спокойный(ая)')
    ext = False

    def __init__(self, name=None, age=None, current_status='Спокойный(ая)'):
        super(Children, self).__init__()
        self.name = name
        self.age = age
        self.current_status = current_status

    def run(self):
        while not self.ext:
            sleep(random.randint(5, 8))
            self.current_status = random.choice(self.status)
            if self.current_status == 'Голодный(ая)' or self.current_status == 'Разбушевался(ась)':
                for _ in range(10):
                    sleep(3)
                    if self.current_status == 'Спокойный(ая)' or self.ext:
                        break
                else:
                    print('Ребёнку конец!')
                    self.ext = True



