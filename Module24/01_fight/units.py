class Warrior:
    count = 1
    heals = 100

    def __init__(self):
        self.name = '{} воин'.format(str(self.count))
        Warrior.count += 1

    def current_heals(self, winner):
        print('{} нанёс удар {}у'.format(winner.name, self.name))
        self.heals -= 20
        print('У {}а осталось {} здоровья.'.format(self.name, self.heals))
        if self.heals == 0:
            print('Победил {} воин'.format(winner.name))
