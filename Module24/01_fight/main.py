import units
from random import choice

first_unit = units.Warrior()
second_unit = units.Warrior()

while (first_unit.heals != 0) and (second_unit.heals != 0):
    choice((
        first_unit.current_heals(second_unit),
        second_unit.current_heals(first_unit)
    ))


