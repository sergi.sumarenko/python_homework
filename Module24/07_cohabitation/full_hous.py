class Human:
    human_satiety = 50

    def __init__(self, name, house):
        self.name = name
        self.house = house
        self.food = self.house.fridge
        self.money = self.house.nightstand

    def to_eat(self):
        lvl_hungry = 50 - self.human_satiety
        if self.food >= lvl_hungry:
            self.human_satiety += lvl_hungry
            self.food -= lvl_hungry
        elif 0 < self.food < lvl_hungry:
            self.human_satiety += self.food
            self.food -= self.food
        else:
            #print('{}:\tЕды нет. Нужно идти в магазин.\n'.format(self.name))
            if self.money <= 0:
                #print('{}:\tНо нет денег, нужно идти на работу.\n'.format(self.name))
                self.to_work()
                self.shopping()
        #print('{}:\tПоел(а), сыт на {} из 50. Еды осталось {}\n'.format(
            #self.name, self.human_satiety, self.food
        #))

    def to_work(self):
        self.human_satiety -= 16
        self.money += 30
        #print('{}:\tСходил на работу, теперь у меня {}$\n'.format(self.name, self.money))

    def to_play(self):
        self.human_satiety -= 10
        #print('{}:\tПоиграл(а) и немного проголодался.\n'.format(self.name))

    def shopping(self):
        if self.money == 0:
            #print('{}:\tДенег на покупки нет.\n'.format(self.name))
            self.to_work()
        else:
            need_food = 50 - self.food
            if self.money > need_food:
                self.money -= need_food
                self.food += need_food
            else:
                self.money -= self.money
                self.food += self.money
            #print('{}:\tСкупился. Теперь у меня {} еды, в тумбочке {}$\n'.format(
                #self.name, self.food, self.money
            #))


class House:
    fridge = 50
    nightstand = 0

