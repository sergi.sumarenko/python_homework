import full_hous
from random import randint
import threading


def try_to_alive(human):
    for day in range(1, 366):
        random_num = randint(1, 6)
        if human.human_satiety < 20 or random_num == 2:
            human.to_eat()
        elif human.food < 10:
            human.shopping()
        elif human.money < 50 or random_num == 1:
            human.to_work()
        else:
            human.to_play()
        if human.human_satiety < 0:
            print('\n!!!{} умер(ла) от голода на {} день...'.format(human.name, day))
            break
    else:
        print('\n{} выжил(а)!'.format(human.name))


they_house = full_hous.House()
artem = full_hous.Human('Артём', they_house)
alisa = full_hous.Human('Алиса', they_house)

artem_live = threading.Thread(target=try_to_alive, args=(artem,))


artem_live.start()
try_to_alive(alisa)

artem_live.join()


